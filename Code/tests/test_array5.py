import numpy as np
import common.constants as c
import math as m


timestep = 2
t = np.arange(0, 1831, timestep).astype(np.float128)
y = np.random.uniform(0, 10, int(1831/timestep) + 1)
l = m.log(2) / c.HALF_LIFE_11C
dt = 1

n = np.exp(-(l / 2) * t) * np.sqrt(y / dt) * np.random.normal(0.0, 1.0, y.shape)
n = np.maximum(n, -y)
print(n)
