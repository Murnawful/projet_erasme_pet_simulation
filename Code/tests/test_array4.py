import numpy as np
import scipy.stats as sc

mean = np.random.uniform(0, 1, 27).reshape(3, 3, 3)
std = np.random.uniform(0, .1, 27).reshape(3, 3, 3)

print(mean)
print(std)
lower, upper = np.amax(mean), np.amin(mean)
nne = np.where(std != 0)
a, b = np.zeros(mean.shape), np.zeros(mean.shape)
a[nne], b[nne] = (lower - mean[nne]) / std[nne], (upper - mean[nne]) / std[nne]
print(a)
print(b)

p = sc.truncnorm.rvs(a, b, loc=mean, scale=std)
print(p)
