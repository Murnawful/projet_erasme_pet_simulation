import SimpleITK as sitk

# Tableaux de probabilités de présence de chaque tissus cervical (WM = white matter, GM = gray matter, CSF = liquide céphalo-rachidien, Vessels = vaisseaux sanguins)
#WM = sitk.GetArrayFromImage(sitk.ReadImage("/media/strangesyd/Seagate/Stage/Atlas/MRA/WM.mha"))
GM = sitk.GetArrayFromImage(sitk.ReadImage("/media/strangesyd/Seagate/Stage/Atlas/MRA/GM.mha"))
#CSF = sitk.GetArrayFromImage(sitk.ReadImage("/media/strangesyd/Seagate/Stage/Atlas/MRA/CSF.mha"))
#Vessels = sitk.GetArrayFromImage(sitk.ReadImage("/media/strangesyd/Seagate/Stage/Atlas/MRA/Vessels.mha"))

"""
img = None
for i in range(GM.shape[0]):
    if img is None:
        img = plt.imshow(GM[i, :, :], vmin=0, vmax=1)
    else:
        img.set_data(GM[i, :, :])
    plt.pause(.05)
    plt.draw()
"""

"""
l = 10 # longueur de frame (le bruit en dépend !)
timestep = 1 # longueur du pas de simulation
ts_ = np.arange(0, 1831 - l, timestep) # vecteur des débuts de frame
te_ = ts_ + l # vecteur des fins de frame
t_ = np.arange(0, 1831, timestep) # vecteur temps pour l'analytique
tf_ = (te_ + ts_)/2 # vecteur temps pour frame, milieu de chaque frame
p = np.load("/media/strangesyd/Seagate/Stage/Feng Parameters.npy") # paramètres de Feng
p[-1] = 30 # le démarrage du scan se passe 30 secondes avant l'injection

if_ = feng.feng_input_function_generate_frame(ts_, te_, *p)
i_ = feng.feng_input_function_generate(t_, *p) # génération de la fonction d'entrée par le modèle de Feng

mod = np.load("/media/strangesyd/Seagate/Stage/LTI Distribution.npy") # paramètres du système
s = mod[4000] # choix d'un set de données au hasard

#sim_o_ = fom.first_order_model_generate(i_, t_, *s) # génération de la sortie du modèle simulé

#plt.plot(t_, sim_o_, '--', label="Sortie du modèle simulé")
plt.plot(t_, i_, '-.', label="Entrée")
plt.plot(tf_, if_, label="Entrée frame")

k2 = 1/s[1]
k1 = s[0]*k2

analf_o_ = otcm.one_tissue_compartmental_model_generate_analytical_frame(ts_, te_, *p, k1, k2, s[3], s[2]) # génération de la sortie du modèle analytique en frame
anal_o_ = otcm.one_tissue_compartmental_model_generate_analytical(t_, *p, k1, k2, s[3], s[2]) # génération de la sortie du modèle analytique

plt.plot(tf_, analf_o_, ':', label="Sortie modèle analytique frame")
plt.plot(t_, anal_o_, '--', label="Sortie modèle analytique")

plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0., prop={'size': 8})

plt.grid(1, which='major', axis='both', color="tab:gray", linestyle=":")

plt.show()
"""