import numpy as np
import math as m
import common.poly as p


def get_origin_mm(center_atlas, spacing, shape):
    """ Fonction renvoyant un tuple contenant les coordonnées en millimètres de l'origine de l'atlas grâce au centre de
    celui-ci (qui est une donnée du problème) """




def compute_poly_3D(coord, coef, degre, typ):
    """ Fonction renvoyant un atlas de valeurs de FWHM interpolée par des polynômes à partir de mesures """
    # coord: array 4D de coordonnées en mm des voxels par rapport au centre du repère
    # coef: array contenant les coefficient des polynômes
    # degree: degré max du polynôme
    # ty: type de polynôme d'interpolation

    # Initialisation de l'atlas à des valeurs nulles
    res = np.zeros(coord.shape[0:3])
    # Séparation des valeurs en millimètres selon chaque axe
    coor_x, coor_y, coor_z = coord[:, :, :, 0], coord[:, :, :, 1], coord[:, :, :, 2]

    # Remplissage de l'atlas en utilisant les polynômes pondérés par les coefficients calculés précédemment
    n = 0
    for nx in range(degre + 1):
        for ny in range(degre + 1 - nx):
            for nz in range(degre + 1 - nx - ny):
                if typ == 'Chebyshev':
                    res += coef[n] * p.chebyshev3D(nx, ny, nz, coor_x, coor_y, coor_z)
                    n += 1
                elif typ == 'Legendre':
                    res += coef[n] * p.legendre3D(nx, ny, nz, coor_x, coor_y, coor_z)
                    n += 1
                else:
                    res += coef[n] * p.canonical3D(nx, ny, nz, coor_x, coor_y, coor_z)
                    n += 1

    return res


def poly_coef(data_c, polynomial_degree, polynomial_type):
    """ Fonction calculant les coefficients de polynômes pour l'interpolation en résolvant un système du type Ax = b """

    # Comptage du nombre de polynômes
    number_of_polynomials = 0
    for nx in range(polynomial_degree + 1):
        for ny in range(polynomial_degree + 1 - nx):
            for nz in range(polynomial_degree + 1 - nx - ny):
                number_of_polynomials += 1

    # Normalisation pour ramener le domaine des polynômes à [-1, 1]
    # TODO: vérifier si c'est nécessaire ou non
    temp = []
    for i in list(data_c.keys()):
        temp.append(i[0])
    x_c = max(temp)
    temp = []
    for i in list(data_c.keys()):
        temp.append(i[1])
    y_c = max(temp)
    temp = []
    for i in list(data_c.keys()):
        temp.append(i[2])
    z_c = max(temp)

    # Génération des polynômes sous forme de matrice A
    polynomial_matrix = np.zeros((len(data_c), number_of_polynomials))
    m = 0
    for (x, y, z) in data_c.keys():
        n = 0
        for nx in range(polynomial_degree + 1):
            for ny in range(polynomial_degree + 1 - nx):
                for nz in range(polynomial_degree + 1 - nx - ny):
                    if polynomial_type == 'Chebyshev':
                        polynomial_matrix[m, n] = p.chebyshev3D(nx, ny, nz, x, y, z)
                    elif polynomial_type == 'Legendre':
                        polynomial_matrix[m, n] = p.legendre3D(nx, ny, nz, x, y, z)
                    else:
                        polynomial_matrix[m, n] = p.canonical3D(nx, ny, nz, x, y, z)
                    n += 1
        m += 1

    # Création du vecteur b
    fwhm_data = np.array(list(data_c.values()))

    # Résolution par les moindres carrés d'un système Ax = b mal déterminé
    sol = np.linalg.lstsq(polynomial_matrix, fwhm_data, rcond=-1)[0]

    return sol


def unpack_data(file_res, file_center):
    """ Fonction récupérant et traitant les données issues de mesures et le centre du volume actif """

    # Ouverture des fichiers .csv
    f1 = open(file_res, 'r')
    f2 = open(file_center, 'r')

    s1 = f1.read().split("\n")
    s2 = f2.read().split("\n")

    f1.close()
    f2.close()

    # La dernière ligne est toujours vide
    d = s1[1:len(s1) - 1]

    # Conversion en array 1D des coordonnées du centre du volume actif
    center_data = s2[-2].split(',')
    center_data = list(filter(lambda a: a != '', center_data))
    center_data = np.array([float(i) for i in center_data])

    # Stockage dans un dictionnaire des mesures
    all_data = {}
    for i in range(len(d)):
        d[i] = d[i].split(",")
        for j in range(len(d[i])):
            # Si il n'y a pas de mesure pour ce point, la valeur est None
            if d[i][j] == '':
                d[i][j] = None
            # Si il y a une mesure, on la converti en float
            else:
                d[i][j] = float(d[i][j])
        # Remplissage du dictionnaire avec en keys les coordonnées en millimètres des points de mesure et en values les
        # données des mesures
        all_data[(d[i][0], d[i][1], d[i][2])] = np.array(d[i][3:])
        # Ajout de points de mesure symétriques pour tenir compte de la symétrie du scanner
        all_data[(-d[i][0], -d[i][1], -d[i][2])] = np.array(d[i][3:])

    # Création d'un dictionnaire contenant en values les coordonnées des points de mesure et en keys leur équivalent en
    # terme de longueur en voxels
    all_data_ingrid = {}
    for (x, y, z) in all_data.keys():
        all_data_ingrid[(int(m.ceil(m.floor(x) / 2)), int(m.ceil(m.floor(y) / 2)), int(m.ceil(m.floor(z) / 2)))] = \
            (x, y, z)

    # Séparation des données selon les axes et élimination des None
    all_data_x, all_data_y, all_data_z = {}, {}, {}
    for (x, y, z), (in_x, in_y, in_z) in all_data.items():
        if in_x is not None:
            all_data_x[(x, y, z)] = in_x
        if in_y is not None:
            all_data_y[(x, y, z)] = in_y
        if in_z is not None:
            all_data_z[(x, y, z)] = in_z

    return all_data_ingrid, all_data_x, all_data_y, all_data_z, center_data


def array_building(v_x, v_y, v_z, ingrid, s):
    """ Fonction construisant un atlas contenant les distances en centre de cet atlas en millimètres """

    # Déduction des dimensions de l'atlas à créer de façon à ce qu'il englobe le volume actif et les mesures
    temp = list(ingrid.keys())
    t_x = abs(max([i[0] for i in temp]) - min([i[0] for i in temp]))
    if t_x >= v_x:
        len_voxel_x = 2 * t_x
    else:
        len_voxel_x = 2 * v_x
    t_y = abs(max([i[1] for i in temp]) - min([i[1] for i in temp]))
    if t_y >= v_y:
        len_voxel_y = 2 * t_y
    else:
        len_voxel_y = 2 * v_y
    t_z = abs(max([i[2] for i in temp]) - min([i[2] for i in temp]))
    if t_z >= v_z:
        len_voxel_z = 2 * t_z
    else:
        len_voxel_z = 2 * v_z

    # Les dimensions de l'atlas
    size_in_voxel = np.array([len_voxel_x, len_voxel_y, len_voxel_z])
    # Les coordonnées du centre de l'atlas
    origin_voxel_centered = (len_voxel_x // 2, len_voxel_y // 2, len_voxel_z // 2)
    # Création de l'atlas
    big_array = np.zeros((len_voxel_z, len_voxel_y, len_voxel_x, 3))

    # Placement des coordonnées en mm dans les voxels
    fwhm_new_x = np.linspace(-len_voxel_x // 2 * s, len_voxel_x // 2 * s, len_voxel_x)
    fwhm_new_y = np.linspace(-len_voxel_y // 2 * s, len_voxel_y // 2 * s, len_voxel_y)
    fwhm_new_z = np.linspace(-len_voxel_z // 2 * s, len_voxel_z // 2 * s, len_voxel_z)
    big_array[:, :, :, 0] = fwhm_new_x
    big_array[:, :, :, 1] = fwhm_new_y[:, np.newaxis]
    big_array[:, :, :, 2] = fwhm_new_z[:, np.newaxis, np.newaxis]

    # Replacement des valeurs en mm connues par mesure dans l'atlas
    for voxel in ingrid.keys():
        coor_voxel_centered = np.array(list(voxel))
        coor_voxel_grid = coor_voxel_centered + size_in_voxel // 2
        big_array[coor_voxel_grid[2], coor_voxel_grid[1], coor_voxel_grid[0], 0] = ingrid[voxel][0]
        big_array[coor_voxel_grid[2], coor_voxel_grid[1], coor_voxel_grid[0], 1] = ingrid[voxel][1]
        big_array[coor_voxel_grid[2], coor_voxel_grid[1], coor_voxel_grid[0], 2] = ingrid[voxel][2]

    return origin_voxel_centered, size_in_voxel, big_array


def restrict(voxel, c, big_array, voxel_size_x, voxel_size_y, voxe_size_z):
    """ Fonction restreignant l'atlas à la région d'intérêt """

    # Déduction des limites de la région à restreindre. Le cas d'un atlas de dimensions impaires est traité
    if voxel_size_x % 2 == 0:
        limx = (int(voxel[0] + c[0] - voxel_size_x // 2),
                int(voxel[0] + c[0] + voxel_size_x // 2))
    else:
        limx = (int(voxel[0] + c[0] - voxel_size_x // 2),
                int(voxel[0] + c[0] + voxel_size_x // 2) + 1)
    if voxel_size_y % 2 == 0:
        limy = (int(voxel[1] + c[1] - voxel_size_y // 2),
                int(voxel[1] + c[1] + voxel_size_y // 2))
    else:
        limy = (int(voxel[1] + c[1] - voxel_size_y // 2),
                int(voxel[1] + c[1] + voxel_size_y // 2) + 1)
    if voxe_size_z % 2 == 0:
        limz = (int(voxel[2] + c[2] - voxe_size_z // 2),
                int(voxel[2] + c[2] + voxe_size_z // 2))
    else:
        limz = (int(voxel[2] + c[2] - voxe_size_z // 2),
                int(voxel[2] + c[2] + voxe_size_z // 2) + 1)

    # Restriction
    coord = big_array[limz[0]:limz[1], limy[0]:limy[1], limx[0]:limx[1], :]

    return coord


def fwhm_poly_interpolation(v_x, v_y, v_z, sampling, degree, ty, path):
    """ Fonction réalisant une interpolation polynômiale de type donné de degré donné des FWHM selon chaque axe sur un
    tableau de  forme (v_z, v_y, v_x) """

    print("Interpolating FWHM data...")
    # Récupération des données de FWHM sous forme de dictionnaires, un pour chaque FWHM
    data_ingrid, data_x, data_y, data_z, origin_mm = unpack_data(path + "/Resolution/reso.csv", path +
                                                                 "/Resolution/origin.csv")

    # Construction d'un atlas contenant les distances à une origine donnée des centres des voxels
    voxel_centered, len_voxel, fwhm = array_building(v_x, v_y, v_x, data_ingrid, sampling)

    # Restriction de l'atlas à la zone correspondant au volume actif
    coor = restrict(voxel_centered, center, fwhm, v_x, v_y, v_z)

    # Conversion du type de polynôme
    if ty == 1:
        ty = "Chebyshev"
    elif ty == 2:
        ty = "Legendre"
    else:
        ty = "Canonical"

    # Calcul des coefficients des polynômes à calculer sur les grilles
    coef_x = poly_coef(data_x, degree, ty)
    coef_y = poly_coef(data_y, degree, ty)
    coef_z = poly_coef(data_z, degree, ty)

    # Récupération des valeurs de FWHM selon chaque axe par interpolation polynômiale
    fwhm_x = compute_poly_3D(coor, coef_x, degree, ty)
    fwhm_y = compute_poly_3D(coor, coef_y, degree, ty)
    fwhm_z = compute_poly_3D(coor, coef_z, degree, ty)
    print("Interpolation complete.")

    return fwhm_x, fwhm_y, fwhm_z



# Test du kernel 3D

import common.visu as v
import scipy.signal as signal
from matplotlib import pyplot as plt


def kernel3d(radius, fwhm_x, fwhm_y, fwhm_z):
    num = 2 * radius + 1

    std_x = fwhm_x / (2 * m.sqrt(2 * m.log(2)))
    std_y = fwhm_y / (2 * m.sqrt(2 * m.log(2)))
    std_z = fwhm_z / (2 * m.sqrt(2 * m.log(2)))

    kernel_1d_x = signal.gaussian(num, std=std_x)
    kernel_1d_y = signal.gaussian(num, std=std_y)
    kernel_1d_z = signal.gaussian(num, std=std_z)
    kernel_2d = np.outer(kernel_1d_y, kernel_1d_x)
    kernel_3d = np.outer(kernel_2d, kernel_1d_z).reshape(num, num, num)
    kernel_3d = np.expand_dims(kernel_3d, -1)
    return kernel_3d


f_x, f_y, f_z = fwhm_poly_interpolation(95, 117, 99, 2, 3, "Legendre")

print(f_x[50, 50, 50])
print(f_y[50, 50, 50])
print(f_z[50, 50, 50])

ker = kernel3d(100, f_x[50, 50, 50], f_y[50, 50, 50], f_z[50, 50, 50])


"""
v.visu_x_save(f_z, 0, 10, 10, "legendre")
v.visu_x_save(f_z1, 0, 10, 10, "chebyshev")
v.visu_x_save(f_z2, 0, 10, 10, "kebab")
v.visu_y_save(f_z, 0, 10, 10, "legendre")
v.visu_y_save(f_z1, 0, 10, 10, "chebyshev")
v.visu_y_save(f_z2, 0, 10, 10, "kebab")
v.visu_z_save(f_z, 0, 10, 10, "legendre")
v.visu_z_save(f_z1, 0, 10, 10, "chebyshev")
v.visu_z_save(f_z2, 0, 10, 10, "kebab")
"""
