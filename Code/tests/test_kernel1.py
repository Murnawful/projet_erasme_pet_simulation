import numpy as np
from scipy import signal, misc
import matplotlib.pyplot as plt

image = misc.ascent()
w = signal.gaussian(50, 10.0)
x = np.arange(0, len(w))
image_new = signal.sepfir2d(image, w, w)

plt.figure()
plt.imshow(image_new)
plt.gray()
plt.title('Original image')
plt.show()
