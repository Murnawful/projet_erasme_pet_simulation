from scipy import signal
import numpy as np
import common.visu as v
from scipy import ndimage as nd


def kernel3d(radius, sigma):
    num = 2 * radius + 1
    kernel_1d = signal.gaussian(num, std=sigma).reshape(num, 1)
    kernel_2d = np.outer(kernel_1d, kernel_1d)
    kernel_3d = np.outer(kernel_1d, kernel_2d).reshape(num, num, num)
    kernel_3d = kernel_3d / kernel_3d.sum()
    kernel_3d = np.expand_dims(kernel_3d, -1)
    return kernel_3d


np.random.seed(1)
s = 10  # scaling factor (original problem: s=10)
nx, ny, nz, nt, nnz = 10 * s, 11 * s, 10 * s, 92 * s, 1000 * s
# select nnz random voxels to fill with time series data
randint = np.random.randint
tseries = {}  # key: (i, j, k) tuple; value: time series data, shape (nt,)
for _ in range(nnz):
    while True:
        ijk = (randint(nx), randint(ny), randint(nz))
        if ijk not in tseries:
            tseries[ijk] = np.random.uniform(0, 1, size=nt)
            break

ijks = np.array(list(tseries.keys()))  # shape (nnz, 3)

# sigmas: key: (i, j, k) tuple; value: standard deviation
sigmas = {k: np.random.uniform(0, 2) for k in tseries.keys()}

# output will be stored as dense array, padded to avoid edge issues
# with convolution.
m = 5  # padding size
cag_4dp = np.zeros((nx + 2 * m, ny + 2 * m, nz + 2 * m, nt))

mm = 2 * m + 1  # kernel width
for (i, j, k), tdata in tseries.items():
    kernel = kernel3d(m, sigmas[(i, j, k)])
    # convolution of one voxel by kernel is trivial.
    # slice4d_c has shape (mm, mm, mm, nt).
    slice4d_c = kernel * tdata
    cag_4dp[i:i + mm, j:j + mm, k:k + mm, :] += slice4d_c

cag_4d = cag_4dp[m:-m, m:-m, m:-m, :]

np.save("/media/strangesyd/Seagate/Stage/test_application_kernel.npy", cag_4d)

"""
# %%

import matplotlib.pyplot as plt

fig, axs = plt.subplots(2, 2, tight_layout=True)
plt.close('all')
# find a few planes
# ks = np.where(np.any(cag_4d != 0, axis=(0, 1,3)))[0]
ks = ijks[:4, 2]
for ax, k in zip(axs.ravel(), ks):
    ax.imshow(cag_4d[:, :, k, nt // 2].T)
    ax.set_title(f'Voxel [:, :, {k}] at time {nt // 2}')

fig.show()

for ijk, sigma in sigmas.items():
    print(f'{ijk}: sigma={sigma:.2f}')
"""
