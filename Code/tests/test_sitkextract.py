import SimpleITK as sitk
import numpy as np
from matplotlib import pyplot as plt
import common.visu as v


parts = 1  # Number of sub-regions we use

file_reader = sitk.ImageFileReader()
file_reader.SetFileName("/media/strangesyd/Seagate/Stage/06/PET High (Registered 50 30 15 -1 50).mha")
file_reader.ReadImageInformation()
image_size = file_reader.GetSize()

file_reader.SetExtractSize((128, 128, 0))
file_reader.SetExtractIndex((0, 0, 50))
temp = file_reader.Execute()

"""result_img = sitk.Image(file_reader.GetSize(), file_reader.GetPixelID(),
                        file_reader.GetNumberOfComponents())
result_img.SetSpacing(file_reader.GetSpacing())
result_img.SetOrigin(file_reader.GetOrigin())
result_img.SetDirection(file_reader.GetDirection())"""

pet = sitk.GetArrayFromImage(temp)[:, :, 500]
print(pet.shape)
plt.imshow(pet, vmin=0, vmax=24000, cmap='gray')
plt.xlabel("x [voxels]")
plt.ylabel("y [voxels]")
plt.show()


"""extract_size = list(file_reader.GetSize())
extract_size[-1] = extract_size[-1] // parts
current_index = [0] * file_reader.GetDimension()
for i in range(parts):
    if i == (parts - 1):
        extract_size[-1] = image_size[-1] - current_index[-1]
    file_reader.SetFileName(image1_file_name)
    file_reader.SetExtractIndex(current_index)
    file_reader.SetExtractSize(extract_size)
    sub_image1 = file_reader.Execute()

    file_reader.SetFileName(image2_file_name)
    file_reader.SetExtractIndex(current_index)
    file_reader.SetExtractSize(extract_size)
    sub_image2 = file_reader.Execute()
    result_img = sitk.Paste(result_img, sub_image1 - sub_image2,
                            extract_size, [0] * file_reader.GetDimension(),
                            current_index)
    current_index[-1] += extract_size[-1]
del sub_image1
del sub_image2"""
