import numpy as np
import OneTissueCompartmentalModelling as otcm
from matplotlib import pyplot as plt

w = np.array([[[.810, .0241], [.1382, .013]], [[.781, .135], [.024, .780]]])
x = np.array([[[.011, .0023], [.5300, .204]], [[.509, .601], [.072, .480]]])
y = np.array([[[.990, .0180], [.0110, .051]], [[.013, .014], [.515, .106]]])
z = np.array([[[.087, .0230], [.0146, .450]], [[.740, .015], [.406, .091]]])

t = np.random.randint(0, 5, (2, 2, 2))

print("t")
print(t)

a = np.stack((w, x, y, z), axis=-1)
a[np.where(t == 0)] = np.array([0, 0, 0, 0])  # atlas des paramètres

print("a")
print(a)
y = np.argwhere(t != 0)  # coordonnées des voxels non vides

print("nnz")
nnz = a[np.where(t != 0)]  # tableau des paramètres pour les voxel non vides
print(nnz)

print("y")
print(y)

tac = np.zeros((len(nnz), 1831))

p = np.load("/media/strangesyd/Seagate/Stage/Feng Parameters.npy")  # paramètres de Feng
p[-1] = 30  # le démarrage du scan se passe 30 secondes avant l'injection
pa = np.zeros((len(nnz), len(p)))
pa[:, :] = p
len_t = 1831
t_ = np.arange(0, len_t, 1)  # vecteur temps pour l'analytique
tac[:, :] = t_
tac = np.concatenate((tac, pa, nnz), axis=-1)

tac_values = np.apply_along_axis(otcm.one_tissue_compartmental_model_generate_analytical, 1, tac)
print(tac_values.shape)

plt.plot(t_[1:], tac_values[1, :])
plt.show()

# nnz = np.arange(1, np.count_nonzero(t) + 1, 1)
# np.place(t, t != 0, nnz)
