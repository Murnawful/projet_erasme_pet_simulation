import numpy as np
import scipy.stats as sc

#np.seterr(divide='ignore', invalid='ignore')


w = np.array([[[.810, .0241], [.1382, .013]], [[.781, .135], [.024, .780]]])
x = np.array([[[.011, .0023], [.5300, .204]], [[.509, .601], [.072, .480]]])
y = np.array([[[.990, .0180], [.0110, .051]], [[.013, .014], [.515, .106]]])
z = np.array([[[.087, .0230], [.0146, .450]], [[.740, .015], [.406, .091]]])

t1 = np.array([.1, .2, .3, .4])
t2 = np.array([.5, .6, .7, .8])
t3 = np.array([.9, .01, .02, .03])
t4 = np.array([.04, .05, .06, .07])

a = np.stack((w, x, y, z), axis=-1)

ind = np.argmax(a, axis=-1)

a_max = np.amax(a, axis=-1)

dim = a.shape

print(ind[:, :, :, np.newaxis].shape)
print(dim)

parameter_map = np.zeros(dim, dtype=np.float32)
white_matter_voxels = np.where(ind == 1)

num_white_matter_voxels = len(white_matter_voxels[0])

mu = 5
sigma = 1
k1_values = sc.truncnorm((0-mu)/sigma, (1-mu)/sigma, loc=mu, scale=sigma).rvs(num_white_matter_voxels)
k1_values = k1_values[:, np.newaxis]
k1_values = np.concatenate((k1_values, np.zeros(k1_values.shape)), axis=-1)
k1_values = np.concatenate((k1_values, np.zeros(k1_values.shape)), axis=-1)

parameter_map[white_matter_voxels] = k1_values
print(parameter_map)

"""
ind = ind[:, :, :, np.newaxis]
ze = np.zeros(ind.shape)
temp = np.concatenate((ind, ze), axis=-1)
ze = np.zeros(temp.shape)
temp = np.concatenate((ind, ze), axis=-1)
ze = np.zeros(temp.shape)
temp = np.concatenate((ind, ze), axis=-1)

temp[np.where(np.sum(temp, axis=-1) == 0)] = t1
temp[np.where(np.sum(temp, axis=-1) == 1)] = t2
temp[np.where(np.sum(temp, axis=-1) == 2)] = t3
temp[np.where(np.sum(temp, axis=-1) == 3)] = t4
"""