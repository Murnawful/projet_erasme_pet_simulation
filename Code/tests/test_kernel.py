import numpy as np
from scipy import signal
from scipy import ndimage as nd
from matplotlib import pyplot as plt


def gaussian_kernel_2d(radius, sigma):
    num = 2 * radius + 1
    kernel_1d = signal.gaussian(num, std=sigma).reshape(num, 1)
    kernel_2d = np.outer(kernel_1d, kernel_1d)
    return kernel_2d


def gaussian_kernel_3d(radius, sigma):
    num = 2 * radius + 1
    kernel_1d = signal.gaussian(num, std=sigma).reshape(num, 1)
    kernel_2d = np.outer(kernel_1d, kernel_1d)
    kernel_3d = np.outer(kernel_1d, kernel_2d).reshape(num, num, num)
    kernel_3d = np.expand_dims(kernel_3d, -1)
    return kernel_3d


np.random.seed(0)

number_of_tac = 150
time_samples = 915
z, y, x = 100, 150, 100
voxel_number = x * y * z

# TACs in the right order
tac = np.random.uniform(0, 4, time_samples * number_of_tac).reshape(number_of_tac, time_samples)

arr = np.array([0] * (voxel_number - number_of_tac) + [1] * number_of_tac)
np.random.shuffle(arr)
arr = arr.reshape(z, y, x)
coor = np.where(arr != 0)  # non-empty voxel

# Algorithm to replace TAC in 3D space
nnz = np.zeros(arr.shape)
nnz[coor] = 1
tac_4d = np.zeros((x, y, z, time_samples))
tac_4d[np.where(nnz == 1)] = tac

# 4D convolution for all time
g = gaussian_kernel_3d(1, .5)  # 3D kernel of std = 1
# v = np.random.uniform(0, 1, x * y * z).reshape(z, y, x)  # 3D array of std
v = (.6, .5, .4, 0)
# cag = nd.convolve(tac_4d, g, mode='constant', cval=0.0)  # convolution
cag = nd.gaussian_filter(tac_4d, sigma=v, mode='constant')
print(tac)
print(cag[coor])

img = None
for i in range(cag.shape[0]):
    if img is None:
        img = plt.imshow(cag[i, :, :, 500], vmin=0, vmax=1, cmap='gray')
    else:
        img.set_data(cag[i, :, :, 500])
    plt.pause(.05)
    plt.draw()


"""
# Convolution d'un array 2D
s = 80
a = np.random.uniform(0, .1, s ** 2).reshape(s, s)
g = gaussian_kernel_2d(1, .5)
cag = nd.convolve(a, g, mode='constant', cval=0.0)
print(a)
print(cag)
f = plt.figure()
f.add_subplot(1, 2, 1)
plt.imshow(a, cmap='gray')
f.add_subplot(1, 2, 2)
plt.imshow(cag, cmap='gray')
plt.show(block=True)
"""

"""
# Convolution d'un array 3D
s = 100
pos = 50
len_t = 30
a = np.random.uniform(0, .1, len_t * s ** 3).reshape(s, s, s, len_t)
g = gaussian_kernel_3d(1, 1)
cag = nd.convolve(a, g, mode='constant', cval=0.0)
img = None
for i in range(cag.shape[-1]):
    if img is None:
        plt.imshow(cag[pos, :, :, i], cmap='gray')
    else:
        img.set_data(cag[pos, :, :, i])
    plt.pause(.05)
    plt.draw()
"""

"""
t = np.random.uniform(0, 4, 14400).reshape(120, 120)
a = nd.gaussian_filter(t, .5)

f = plt.figure()
f.add_subplot(1, 2, 1)
plt.imshow(t, cmap='gray')
f.add_subplot(1, 2, 2)
plt.imshow(a, cmap='gray')
plt.show(block=True)
"""
