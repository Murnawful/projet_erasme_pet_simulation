import numpy as np


def ex(x):
    return 0


t = np.arange(0, 5)
a1 = np.random.randint(0, 100, 30)
k1 = np.random.randint(0, 100, 30)
k2 = np.random.randint(0, 100, 30)
d = np.random.randint(0, 100, 30)
l1 = 2
len_p = len(a1)
len_t = len(t)
print(t)
print(a1)
print(k1)
print(k2)
print(d)


a1 = a1[:, np.newaxis]
k1 = k1[:, np.newaxis]
k2 = k2[:, np.newaxis]
d = d[:, np.newaxis]
print(k1.shape)

y = (a1*(t - d)*k1)/(k2+l1)*np.exp(l1*(t - d))
print(y.shape)

y_arr = np.zeros((len_p, len_t))
for col_idx in range(0, len_t):
    y_arr[:, col_idx] = (a1*(t[col_idx] - d)*k1)/(k2+l1)*np.exp(l1*(t[col_idx] - d))

print(y_arr.shape)
