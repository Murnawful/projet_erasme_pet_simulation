import numpy as np
import algo_1C.one_tissue_compartmental_modelling as otcm


# Variable globale du chemin vers les données
path_to_data = ""


def generate_analytical_frame(ts_, te_, at, ind, feng):
    """ Fonction générant les TAC noiseless analytiques moyennées sur une frame en utilisant les paramètres
    pharmacocinétiques stockés dans l'atlas 'at' """

    # Coordonnées des voxels non vides
    coor = np.nonzero(ind)
    # Récupération des paramètres pharmacocinétiques correspondant aux coordonnées des voxels non vides
    nnz = at[coor]

    # Assignation des paramètres pharmacocinétiques
    k1 = nnz[:, 0][:, np.newaxis]
    k2 = nnz[:, 1][:, np.newaxis]
    delay = nnz[:, 2][:, np.newaxis]
    alpha = nnz[:, 3][:, np.newaxis]
    p = (k1, k2, delay, alpha)

    print("Generating curves...")
    # Création des TAC noiseless moyennées sur une frame
    noiseless_tac = otcm.one_tissue_compartmental_model_generate_analytical_frame(ts_, te_, *feng, *p)
    print("Complete")

    return noiseless_tac, coor


def generate_analytical(t_, at, ind, feng):
    """ Fonction générant les TAC noiseless analytiques en utilisant les paramètres pharmacocinétiques stockés dans
    l'atlas 'at' """

    # Coordonnées des voxels non vides !! format (z, y, x) !!
    coor = np.nonzero(ind)
    # Récupération des paramètres pharmacocinétiques correspondant aux coordonnées des voxels non vides
    nnz = at[coor]

    # Assignation des paramètres pharmacocinétiques
    k1 = nnz[:, 0][:, np.newaxis]
    k2 = nnz[:, 1][:, np.newaxis]
    delay = nnz[:, 2][:, np.newaxis]
    alpha = nnz[:, 3][:, np.newaxis]
    p = (k1, k2, delay, alpha)

    print("Generating curves...")
    # Création des TAC noiseless
    noiseless_tac = otcm.one_tissue_compartmental_model_generate_analytical(t_, *feng, *p)
    print("Complete")

    return noiseless_tac, coor


def load_all(a):
    """ Fonction récupérant les atlas """

    # Récupération de l'atlas des paramètres
    at = np.load(path_to_data + "/Atlas/" + a + "/" + a + "_param1C_atlas_max.npy").astype(np.float32)
    # Récupération de l'atlas des indices de tissus pour chaque voxel
    at_ind = np.load(path_to_data + "/Atlas/" + a + "/" + a + "_prob_atlas_ind_max.npy")
    # Récupération des paramètres de Feng
    feng = np.load(path_to_data + "/Feng Parameters.npy").astype(np.float32)
    # Le démarrage du scan se passe 30 secondes avant l'injection
    feng[-1] = 30
    return at, at_ind, feng


def write_to_log_file(at, frame):
    """ Fonction écrivant les métadonnées de courbes noiseless en les ajoutant à un fichier existant """

    # Ouverture du log en mode 'append'
    f = open(path_to_data + "/Atlas/" + at + "/" + at + "_TAC_metadata.txt", 'w+')
    f.write("Frame length = " + str(frame) + "\n")
    f.close()


def save_tac_frame(frame, at, t_, tac, coor):
    """ Fonction stockant les TAC noiseless moyennées sur une frame temporelle de longueur déterminée non placées dans
    l'atlas, leurs coordonnées dans l'atlas et le vecteur temps """

    print("Saving curves for " + at + " atlas...")

    # Stockage des TAC noiseless
    np.save(path_to_data + "/Atlas/" + at + "/" + at + "_TAC_1C_noiseless_frame.npy", tac)
    # Stockage des coordonnées des TAC dans l'atlas
    np.save(path_to_data + "/Atlas/" + at + "/" + at + "_TAC_1C_coor.npy", coor)
    # Stockage du vecteur temps
    np.save(path_to_data + "/Atlas/" + at + "/" + at + "_TAC_1C_time.npy", t_)

    write_to_log_file(at, frame)

    print("Saving complete")


def save_tac(at, t_, tac, coor):
    """ Fonction stockant les TAC noiseless analytiques non placées dans l'atlas, leurs coordonnées dans l'atlas
    et le vecteur temps """

    print("Saving curves for " + at + " atlas...")
    # Stockage des TAC noiseless
    np.save(path_to_data + "/Atlas/" + at + "/" + at + "_TAC_1C_noiseless.npy", tac)
    # Stockage des coordonnées des TAC dans l'atlas
    np.save(path_to_data + "/Atlas/" + at + "/" + at + "_TAC_1C_coor.npy", coor)
    # Stockage du vecteur temps
    np.save(path_to_data + "/Atlas/" + at + "/" + at + "_TAC_1C_time.npy", t_)
    print("Saving complete")


def menu(path):
    """ MENU """

    # Récupération de l'arborescence
    global path_to_data
    path_to_data = path

    print("Available Atlas:\n"
          "1 - MRA\n"
          "2 - TOF\n"
          "3 - UBA167\n"
          "4 - All\n"
          "0 - Exit")
    ch = int(input("For which atlas would you like to generate noiseless TAC? "))

    while ch != 0:
        # Longueur du pas de simulation
        timestep = float(input("Time step (typical value: 2): "))
        # Durée de la simulation en seconde
        len_t = float(input("Length of simulation (typical value: 1830): ")) + 1
        fr = input("Average curve on frame length? (n/y) ")

        if fr == "y":
            le = int(input("Length of frame: "))  # longueur de frame (le bruit en dépend !)
            ts_ = np.arange(0, len_t - le, timestep)  # vecteur des débuts de frame
            te_ = ts_ + le  # vecteur des fins de frame
            tf_ = (te_ + ts_) / 2  # vecteur temps pour frame, milieu de chaque frame
            if ch == 1:
                save_tac_frame(le, "MRA", tf_, *generate_analytical_frame(ts_, te_, *load_all("MRA")))
            elif ch == 2:
                save_tac_frame(le, "TOF", tf_, *generate_analytical_frame(ts_, te_, *load_all("TOF")))
            elif ch == 3:
                save_tac_frame(le, "UBA167", tf_, *generate_analytical_frame(ts_, te_, *load_all("UBA167")))
            else:
                save_tac_frame(le, "MRA", tf_, *generate_analytical_frame(ts_, te_, *load_all("MRA")))
                save_tac_frame(le, "TOF", tf_, *generate_analytical_frame(ts_, te_, *load_all("TOF")))
                save_tac_frame(le, "UBA167", tf_, *generate_analytical_frame(ts_, te_, *load_all("UBA167")))
        else:
            t_ = np.arange(0, len_t, timestep)  # vecteur temps pour l'analytique
            if ch == 1:
                save_tac("MRA", t_, *generate_analytical(t_, *load_all("MRA")))
            elif ch == 2:
                save_tac("TOF", t_, *generate_analytical(t_, *load_all("TOF")))
            elif ch == 3:
                save_tac("UBA167", t_, *generate_analytical(t_, *load_all("UBA167")))
            else:
                save_tac("MRA", t_, *generate_analytical(t_, *load_all("MRA")))
                save_tac("TOF", t_, *generate_analytical(t_, *load_all("TOF")))
                save_tac("UBA167", t_, *generate_analytical(t_, *load_all("UBA167")))
        print("Available Atlas:\n"
              "1 - MRA\n"
              "2 - TOF\n"
              "3 - UBA167\n"
              "4 - All\n"
              "0 - Exit")
        ch = int(input("For which atlas would you like to generate noiseless TAC? "))

