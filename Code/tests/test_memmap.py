import numpy as np
import math as m


path = "/media/strangesyd/Seagate/Stage"

t_ = np.load(path + "/Atlas/MRA/MRA_TAC_1C_time.npy")
coor = np.load(path + "/Atlas/MRA/MRA_TAC_1C_coor.npy")
f = open(path + "/Atlas/MRA/MRA_atlas_metadata.txt", 'r')
tac_n = np.load(path + "/Atlas/MRA/MRA_TAC_1C_noiseless_frame.npy")
tac_r = np.load(path + "/Atlas/MRA/MRA_TAC_1C_real_frame.npy")
shape = f.read().split("\n")
shape = shape[1][31:-1].split(", ")
shape = (int(shape[0]), int(shape[1]), int(shape[2]))

tac_noiseless = np.memmap(path + "/Atlas/MRA/MRA_TAC_1C_noiseless_frame_complete.npy", dtype='float64', mode='r',
                          shape=(shape[0], shape[1], shape[2], len(t_)))
tac_real = np.memmap(path + "/Atlas/MRA/MRA_TAC_1C_real_frame_complete.npy", dtype='float64', mode='r', shape=(shape[0],
                    shape[1], shape[2], len(t_)))

np.random.seed(51230)
num = np.random.randint(0, len(coor[0, :]))
z, y, x = coor[0, num], coor[1, num], coor[2, num]

print(x, y, z)
ta = tac_n[num, :]
tb = tac_r[num, :]
t1 = tac_noiseless[z, y, x, :]
t2 = tac_real[z, y, x, :]
if False in (ta == t1):
    print("Problème sur les noiseless")
if False in (tb == t2):
    print("Problème sur les réelles")

del tac_noiseless
del tac_real
