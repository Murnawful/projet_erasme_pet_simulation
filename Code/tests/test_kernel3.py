import common.pve_generator as pve
import common.fwhm_computing as fwhm
import numpy as np


at = "MRA"
path = "/media/strangesyd/Seagate/Stage"

t_ = np.load(path + "/Atlas/" + at + "/" + at + "_TAC_1C_time.npy")
coor = np.load(path + "/Atlas/" + at + "/" + at + "_TAC_1C_coor.npy")
f = open(path + "/Atlas/" + at + "/" + at + "_atlas_metadata.txt", 'r')
tac_n = np.load(path + "/Atlas/" + at + "/" + at + "_TAC_1C_noiseless_frame.npy")
# tac_r = np.load(path + "/Atlas/" + at + "/" + at + "_TAC_1C_real_frame.npy")
shape = f.read().split("\n")
shape = shape[1][31:-1].split(", ")
shape = (int(shape[0]), int(shape[1]), int(shape[2]))

sampling = (2, 2, 2)
ty = 1
"""nnz = np.nonzero(tac_n)
print(type(nnz))
print(np.shape(nnz))
print(nnz)
print(type(coor))
print(np.shape(coor))
print(coor)
print(tuple(coor))"""
fwhm_data = fwhm.fwhm_poly_interpolation(shape[0], shape[1], shape[2], sampling, 2, ty, path)
tac_real = pve.add_pve(coor, tac_n, shape, 2, fwhm_data, sampling)
print(tac_real.shape)
np.save(path + "/Atlas/" + at + "/" + at + "_TAC_1C_real_frame.npy", tac_real[tuple(coor)])


"""ke = pve.kernel3d(1, 2, 3, 4, (1, 2, 3))
t_ = np.linspace(0, 10, 11)
res = ke * t_
print(t_)
print(res)"""
