import numpy as np
from scipy.stats import truncnorm
import scipy.integrate as integrate
from matplotlib import pyplot as plt


def integrand(x, mean, std, a1, b1):
    return truncnorm.pdf(x, a1, b1, loc=mean[0], scale=std[0])


mod_min = np.load("/media/strangesyd/Seagate/Stage/mod_kkda_min.npy")
mod_max = np.load("/media/strangesyd/Seagate/Stage/mod_kkda_max.npy")
mod_mean = np.load("/media/strangesyd/Seagate/Stage/mod_kkda_mean.npy")
mod_std = np.load("/media/strangesyd/Seagate/Stage/mod_kkda_std.npy")

print(mod_min.shape)

a, b = (mod_min[2] - mod_mean[2]) / mod_std[2], (mod_max[2] - mod_mean[2]) / mod_std[2]
x_range = np.linspace(-0.005, 50, 1000)
n = integrate.quad(integrand, -18, 18, args=(mod_mean, mod_std, a, b))
scale = n[0] - n[1]
print(n)
plt.plot(x_range, truncnorm.pdf(x_range, a, b, loc=mod_mean[2], scale=mod_std[2]) / n[0])
plt.grid()
plt.xlabel("$K_1$ [mL min$^{-1}$]")
plt.show()
