import numpy as np
import scipy.stats as sc

# mod = np.load("/media/strangesyd/Seagate/Stage/LTI Distribution.npy")  # paramètres du système
mod = np.array([[1, 5, 6, 8],
                [6, 2, 9, 8],
                [4, 7, 8, 9],
                [2, 1, 5, 7],
                [6, 9, 2, 1],
                [3, 4, 1, 4]])

scale = 2
mod1 = np.zeros(mod.shape)
mod1[:, 0] = (mod[:, 0] * scale) / (mod[:, 1] / scale)  # valeurs de k1
mod1[:, 1] = 1 / (mod[:, 1] / scale)  # valeurs de k2
mod1[:, 2] = mod[:, 2]  # valeurs de d
mod1[:, 3] = mod[:, 3]   # valeurs de alpha

mod1_mean = np.mean(mod1, axis=0)
mod1_std = np.std(mod1, axis=0)
print(mod1)
print(mod1_mean)
print(mod1_std)
num = 10

k1_values = sc.truncnorm((0 - mod1_mean[0]) / mod1_std[0], (1 - mod1_mean[0]) / mod1_std[0],
                         loc=mod1_mean[0], scale=mod1_std[0]).rvs(num)
k2_values = sc.truncnorm((0 - mod1_mean[1]) / mod1_std[1], (1 - mod1_mean[1]) / mod1_std[1],
                         loc=mod1_mean[1], scale=mod1_std[1]).rvs(num)
d_values = sc.truncnorm((0 - mod1_mean[2]) / mod1_std[2], (1 - mod1_mean[2]) / mod1_std[2],
                        loc=mod1_mean[2], scale=mod1_std[2]).rvs(num)
alpha_values = sc.truncnorm((0 - mod1_mean[3]) / mod1_std[3], (1 - mod1_mean[3]) / mod1_std[3], loc=mod1_mean[3],
                            scale=mod1_std[3]).rvs(num)
print(k1_values)