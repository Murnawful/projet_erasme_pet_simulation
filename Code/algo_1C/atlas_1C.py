import SimpleITK as sitk
import numpy as np
import scipy.stats as sc
import math

# Variable globale du chemin vers les données
path_to_data = ""


def resample(args, s):
    """ Fonction permettant de resampler une image SITK 'args' à un sampling correspondant à 's' """

    input_image = sitk.ReadImage(args)
    input_image_direction = input_image.GetDirection()
    input_image_origin = input_image.GetOrigin()
    input_image_spacing = input_image.GetSpacing()
    input_image_size = input_image.GetSize()

    resampled_input_image_direction = input_image_direction
    resampled_input_image_origin = input_image_origin
    resampled_input_image_spacing = (s[0], s[1], s[2])
    resampled_input_image_size = [
        int(math.ceil(input_image_size[i] * (input_image_spacing[i] / resampled_input_image_spacing[i]))) for i in
        range(3)]
    input_resampler = sitk.ResampleImageFilter()
    input_resampler.SetInterpolator(sitk.sitkLinear)
    input_resampler.SetOutputDirection(resampled_input_image_direction)
    input_resampler.SetOutputOrigin(resampled_input_image_origin)
    input_resampler.SetOutputSpacing(resampled_input_image_spacing)
    input_resampler.SetSize(resampled_input_image_size)
    resampled_input_image = input_resampler.Execute(input_image)
    return resampled_input_image


def from_patient_to_kkda(tis):
    """ Fonction renvoyant les paramètres pharmacocinétiques au format K1, k2, delay, alpha d'un tissu donné à partir
    des données d'un patient """

    # Matière Blanche au format G, τ, delay, alpha
    if tis == "wm":
        mod_gtda_mean = np.load(path_to_data + "/patients/type1/wm_mean.npy")
        mod_gtda_std = np.load(path_to_data + "/patients/type1/wm_std.npy")
    # Matière Grise au format G, τ, delay, alpha
    elif tis == "gm":
        mod_gtda_mean = np.load(path_to_data + "/patients/type1/gm_mean.npy")
        mod_gtda_std = np.load(path_to_data + "/patients/type1/gm_std.npy")
    # Liquide Céphalo-Rachidien au format G, τ, delay, alpha
    elif tis == "csf":
        mod_gtda_mean = np.array([0, 1, 1, 0])
        mod_gtda_std = np.array((0, 0, 0, 0))
    # Vaisseaux sanguins au format G, τ, delay, alpha
    else:
        mod_gtda_mean = np.array([0, 0, 1, 0])
        mod_gtda_std = np.array([0, 0, 0, 0])

    # Création des arrays de paramètres
    mod_kkda_mean = np.zeros(mod_gtda_mean.shape)
    mod_kkda_std = np.zeros(mod_gtda_std.shape)

    # Transformation du format G, τ, delay, alpha au format K1, k2, delay, alpha
    # Valeurs de K1
    mod_kkda_mean[0] = mod_gtda_mean[0] / mod_gtda_mean[1]
    mod_kkda_std[0] = mod_gtda_std[0] / mod_gtda_std[1]
    # Valeurs de k2
    mod_kkda_mean[1] = 1 / mod_gtda_mean[1]
    mod_kkda_std[1] = 1 / mod_gtda_std[1]
    # Valeurs de d
    mod_kkda_mean[2] = mod_gtda_mean[2]
    mod_kkda_std[2] = mod_gtda_std[2]
    # Valeurs de alpha
    mod_kkda_mean[3] = mod_gtda_mean[3]
    mod_kkda_std[3] = mod_gtda_std[3]

    return mod_kkda_mean, mod_kkda_std


def from_lti_to_kkda(tis):
    """ Fonction renvoyant TOUS les paramètres pharmacocinétiques issus d'une distribution LTI en format K1, k2,
    delay, alpha """

    # La distribution LTI donne des valeurs moyennes sans distinction des différents tissus, le scaling permet d'établir
    # une distinction
    if tis == "wm":  # Matière Blanche
        scale = 1 / 10
    elif tis == "gm":  # Matière Grise
        scale = 2
    elif tis == "csf":  # Liquide Céphalo-Rachidien
        scale = 4
    else:  # Vaisseaux Sanguins
        scale = 1

    # Récupération des paramètres au format G, τ, delay, alpha
    mod_gtda = np.load(path_to_data + "/LTI Distribution.npy")

    # Transformation des paramètres au format K1, k2, delay, alpha
    mod_kkda = np.zeros(mod_gtda.shape)
    # Valeurs de K1
    mod_kkda[:, 0] = (mod_gtda[:, 0] * scale) / (mod_gtda[:, 1] / scale)
    # Valeurs de k2
    mod_kkda[:, 1] = 1 / (mod_gtda[:, 1] / scale)
    # Valeurs de d
    mod_kkda[:, 2] = mod_gtda[:, 2]
    # Valeurs de alpha
    mod_kkda[:, 3] = mod_gtda[:, 3]
    return mod_kkda


def choose_parameter_1C_sum(at):
    mod_kkda_wm = from_lti_to_kkda("wm")
    mod_kkda_wm_mean = np.mean(mod_kkda_wm, axis=0)[np.newaxis, :]  # valeurs moyennes des paramètres pour WM
    mod_kkda_wm_std = np.std(mod_kkda_wm, axis=0)[np.newaxis, :]  # déviations standards des paramètres pour WM
    mod_kkda_wm_max = np.amax(mod_kkda_wm, axis=0)
    mod_kkda_wm_min = np.amin(mod_kkda_wm, axis=0)

    mod_kkda_gm = from_lti_to_kkda("gm")
    mod_kkda_gm_mean = np.mean(mod_kkda_gm, axis=0)[np.newaxis, :]  # valeurs moyennes des paramètres pour GM
    mod_kkda_gm_std = np.std(mod_kkda_gm, axis=0)[np.newaxis, :]  # déviations standards des paramètres pour GM
    mod_kkda_gm_max = np.amax(mod_kkda_gm, axis=0)
    mod_kkda_gm_min = np.amin(mod_kkda_gm, axis=0)

    mod_kkda_csf = from_lti_to_kkda("csf")
    mod_kkda_csf_mean = np.mean(mod_kkda_csf, axis=0)[np.newaxis, :]  # valeurs moyennes des paramètres pour CSF
    mod_kkda_csf_std = np.std(mod_kkda_csf, axis=0)[np.newaxis, :]  # déviations standards des paramètres pour CSF
    mod_kkda_csf_max = np.amax(mod_kkda_csf, axis=0)
    mod_kkda_csf_min = np.amin(mod_kkda_csf, axis=0)

    mod_kkda_vessels = from_lti_to_kkda("vessels")
    mod_kkda_vessels_mean = np.mean(mod_kkda_vessels, axis=0)[np.newaxis, :]  # valeurs moyennes des paramètres pour
    # Vessels
    mod_kkda_vessels_std = np.std(mod_kkda_vessels, axis=0)[np.newaxis, :]  # déviations standards des paramètres pour
    # Vessels
    mod_kkda_vessels_max = np.amax(mod_kkda_vessels, axis=0)
    mod_kkda_vessels_min = np.amin(mod_kkda_vessels, axis=0)

    # Moyenne des paramètres donnés
    mod_kkda_mean = np.concatenate((mod_kkda_wm_mean, mod_kkda_gm_mean, mod_kkda_csf_mean, mod_kkda_vessels_mean),
                                   axis=0)
    # Déviation standard des paramètres donnés
    mod_kkda_std = np.concatenate((mod_kkda_wm_std, mod_kkda_gm_std, mod_kkda_csf_std, mod_kkda_vessels_std),
                                  axis=0)

    param = np.zeros(at.shape)  # Initialisation du tableau de paramètres
    for i in range(4):
        p_mean = np.dot(at, mod_kkda_mean[:, i])[:, :, :, np.newaxis]  # Somme des moyennes pondérée par les probas
        p_std = np.dot(at, mod_kkda_std[:, i])[:, :, :, np.newaxis]  # Somme des déviations pondérée par les probas
        lower = min(mod_kkda_wm_min[i], mod_kkda_gm_min[i], mod_kkda_csf_min[i], mod_kkda_vessels_min[i])
        upper = max(mod_kkda_wm_max[i], mod_kkda_gm_max[i], mod_kkda_csf_max[i], mod_kkda_vessels_max[i])
        a, b = np.zeros(p_mean.shape), np.zeros(p_mean.shape)
        nne = np.where(p_std != 0)
        a[nne] = (lower - p_mean[nne]) / p_std[nne]
        b[nne] = (upper - p_mean[nne]) / p_std[nne]
        p = np.zeros(p_mean.shape)
        p[nne] = sc.truncnorm.rvs(a, b, loc=p_mean, scale=p_std, random_state=1)
        print(p.shape)
    return 0


def choose_parameter_1C_max(tis, num, choice):
    """ Fonction choisissant renvoyant un nombre 'num' de paramètres pharmacocinétiques du tissus 'tis' en les tirant
    d'une loi normale """

    # Si ça vient d'une distribution LTI, récupérer les datas et en calculer la moyenne et le std ainsi que la taille
    # de fenêtre
    if choice == 1:
        # Récupération de TOUS les paramètres de la distribution LTI
        mod_kkda = from_lti_to_kkda(tis)

        # Calcul des moyennes des paramètres sur la distribution
        mod_kkda_mean = np.mean(mod_kkda, axis=0)
        # Calcul des déviations standards des paramètres sur la distribution
        mod_kkda_std = np.std(mod_kkda, axis=0)

        # Calcul de la fenêtre sur laquelle restreindre le choix des paramètres pharmacocinétiques
        mod_kkda_max = np.amax(mod_kkda, axis=0)
        mod_kkda_min = np.amin(mod_kkda, axis=0)

        # Le sang est un peu différent et il vaut mieux le définir à la main pour les tissus
        if tis == "wm":
            alpha_mean = 0.05
        elif tis == "gm":
            alpha_mean = 0.07
        elif tis == "csf":
            alpha_mean = 0.001
        else:
            alpha_mean = 0.8

        mod_kkda_mean[3] = alpha_mean
    # Si ça vient d'un patient, il n'y a qu'à lire les données, la fenêtre est pour le moment arbitraire
    else:
        # TODO: ne plus rendre la fenêtre arbitraire
        mod_kkda_mean, mod_kkda_std = from_patient_to_kkda(tis)
        mod_kkda_max = np.array([100, 100, 1, 100])
        mod_kkda_min = np.array([10, 10, 0, 10])

    # Générer un nombre 'num' de valeurs de paramètres issus d'une distribution normale autour d'une moyenne pour une
    # std donnée et sur une fenêtre donnée
    k1_values = sc.truncnorm((mod_kkda_min[0] - mod_kkda_mean[0]) / mod_kkda_std[0], (mod_kkda_max[0] -
                                                                                      mod_kkda_mean[0]) /
                             mod_kkda_std[0], loc=mod_kkda_mean[0], scale=mod_kkda_std[0]).rvs(num)
    k2_values = sc.truncnorm((mod_kkda_min[1] - mod_kkda_mean[1]) / mod_kkda_std[1], (mod_kkda_max[1] -
                                                                                      mod_kkda_mean[1]) /
                             mod_kkda_std[1], loc=mod_kkda_mean[1], scale=mod_kkda_std[1]).rvs(num)
    d_values = sc.truncnorm((mod_kkda_min[2] - mod_kkda_mean[2]) / mod_kkda_std[2], (mod_kkda_max[2] -
                                                                                     mod_kkda_mean[2]) /
                            mod_kkda_std[2], loc=mod_kkda_mean[2], scale=mod_kkda_std[2]).rvs(num)
    alpha_values = sc.truncnorm((mod_kkda_min[3] - mod_kkda_mean[3]) / mod_kkda_std[3], (mod_kkda_max[3] -
                                                                                         mod_kkda_mean[3]) /
                                mod_kkda_std[3], loc=mod_kkda_mean[3], scale=mod_kkda_std[3]).rvs(num)

    return k1_values, k2_values, d_values, alpha_values


def get_param_max(tis, num, choice):
    """ Fonction récupérant un nombre 'num' de paramètres pharmacocinétiques correspondant au tissu 'tis' """

    sele = choose_parameter_1C_max(tis, num, choice)
    k1 = sele[0][:, np.newaxis]
    k2 = sele[1][:, np.newaxis]
    d = sele[2][:, np.newaxis]
    alpha = sele[3][:, np.newaxis]
    return np.concatenate((k1, k2, d, alpha), axis=1)


def get_data(at):
    """ Fonction récupérant les atlas des tissus """
    """ Requière des atlas de tissus aux bonnes arborescences """

    p = input("Resample? (n/y) ")
    if p == "y":
        s = (float(input("Sample size along x? ")),
             float(input("Sample size along y? ")),
             float(input("Sample size along z? ")))
        print("Resampling...")
        wm = sitk.GetArrayFromImage(resample(path_to_data + "/Atlas/" + at + "/WM.mha", s))
        gm = sitk.GetArrayFromImage(resample(path_to_data + "/Atlas/" + at + "/GM.mha", s))
        csf = sitk.GetArrayFromImage(resample(path_to_data + "/Atlas/" + at + "/CSF.mha", s))
        vessels = sitk.GetArrayFromImage(resample(path_to_data + "/Atlas/" + at + "/Vessels.mha", s))
        print("Done")
    else:
        print("Loading atlas...")
        s = 1
        wm = sitk.GetArrayFromImage(sitk.ReadImage(path_to_data + "/Atlas/" + at + "/WM.mha"))
        gm = sitk.GetArrayFromImage(sitk.ReadImage(path_to_data + "/Atlas/" + at + "/GM.mha"))
        csf = sitk.GetArrayFromImage(sitk.ReadImage(path_to_data + "/Atlas/" + at + "/CSF.mha"))
        vessels = sitk.GetArrayFromImage(sitk.ReadImage(path_to_data + "/Atlas/" + at + "/Vessels.mha"))
        print("Done")

    vessels = vessels > .1  # trick pour faire plus apparaître les petits vaisseaux en sortant un booléen qui
    # correspond à un 1 ou 0
    # c'est nécessaire pour ne pas perdre l'information sur les petits vaisseaux
    return wm, gm, csf, vessels, s


def get_atlas_transport_sum(at):
    # TODO: finir le taf
    choose_parameter_1C_sum(at)
    return 0


def get_atlas_transport_max(ind, sh):
    """ Fonction renvoyant un atlas de paramètres pharmacocinétiques en se basant sur la probabilité maximum du tissus
    dans un voxel """

    #  Création d'un array de la bonne taille
    parameter_map = np.zeros(sh, dtype=np.float32)

    # Coordonnées des voxels de chaque type de tissus
    white_matter_voxels = np.where(ind == 1)
    grey_matter_voxels = np.where(ind == 2)
    csf_voxels = np.where(ind == 3)
    vessels_voxels = np.where(ind == 4)

    # Comptage du nombre de voxels correspondant à chaque type de tissus
    num_white_matter_voxels = len(white_matter_voxels[0])
    num_grey_matter_voxels = len(grey_matter_voxels[0])
    num_csf_voxels = len(csf_voxels[0])
    num_vessels_voxels = len(vessels_voxels[0])

    # Choisir la source du choix des paramètres pharmacocinétiques
    print("Get tissue parameters from:\n"
          "1 - LTI distribution\n"
          "2 - Patient")
    choice = int(input("Choose a source of tissue parameters data: "))

    # Récupération d'un nombre donné de paramètres pour la matière blanche
    p_wm = get_param_max("wm", num_white_matter_voxels, choice)
    parameter_map[white_matter_voxels] = p_wm

    # Récupération d'un nombre donné de paramètres pour la matière grise
    p_gm = get_param_max("gm", num_grey_matter_voxels, choice)
    parameter_map[grey_matter_voxels] = p_gm

    # Récupération d'un nombre donné de paramètres pour le liquide céphalo-rachidien
    p_csf = get_param_max("csf", num_csf_voxels, choice)
    parameter_map[csf_voxels] = p_csf

    # Récupération d'un nombre donné de paramètres pour les vaisseaux sanguins
    p_vessels = get_param_max("vessels", num_vessels_voxels, choice)
    parameter_map[vessels_voxels] = p_vessels

    return parameter_map


def get_atlas(at, choice):
    """ Fonction renvoyant différents atlas basés sur des données sur disque """

    # récupération des atlas de tissus et du sampling (= p)
    wm, gm, csf, vessels, p = get_data(at)
    # rassemblement des atlas de tissus
    atlas = np.stack((wm, gm, csf, vessels), axis=-1).astype(np.float32)
    # récupère l'indice des tissus majoritaires dans chaque voxel
    atlas_ind = np.argmax(atlas, axis=-1)
    # somme toutes les valeurs du dernier axe et remplace cet axe par la somme
    atlas_sum = np.sum(atlas, axis=-1)
    # pour que le fond soit à 0 et que le premier indice soit à 1, du coup les indices sont décalés
    atlas_ind[np.where(atlas_sum != 0)] += 1  # 1 = wm, 2 = gm, 3 = csf, 4 = vessels

    # Retour de la fonction en fonction du choix utilisateur de création d'atlas
    if choice == 0:
        # Renvoie l'atlas complet des probabilités de tissus
        return atlas, p
    elif choice == 1:
        # Renvoie l'atlas contenant les indices des tissus de probabilités maximum dans chaque voxel
        return atlas_ind, p
    elif choice == 2:
        # Renvoie un atlas de tissus dont les probabilités sont normalisées au cas où elles ne le seraient pas
        # même chose que pour atlas_sum mais en gardant les même dimensions pour normaliser l'atlas
        atlas_sum_norm = np.sum(atlas, axis=-1, keepdims=True)
        # normalisation en gardant à 0 ce qui est en-dehors des tissus
        return np.divide(atlas, atlas_sum_norm, out=np.zeros_like(atlas), where=atlas_sum_norm != 0), p
    elif choice == 3:
        # Renvoie un atlas de paramètres pharmacocinétiques
        # Choix de la méthode de déduction des paramètres
        print("Available methods for parameters selection:\n"
              "1 - Maximum method\n"
              "2 - Weighted sum method")
        method = int(input("Choose a method: "))
        if method == 1:
            # Renvoie les paramètres du modèle algo_1C déduits à partir de la probabilité maximum dans le voxel
            return get_atlas_transport_max(atlas_ind, atlas.shape), method, p
        else:
            # Renvoie les paramètres du modèle algo_1C déduits à partir de la somme pondérée des paramètres associés aux
            # probabilités des tissus
            return get_atlas_transport_sum(atlas), method, p


def write_log_to_file(at, method, sampling, typ, shape):
    """ Fonction d'écriture d'un log pour le stockage d'un atlas de paramètres pharmacocinétiques """

    f = open(path_to_data + "/Atlas/" + at + "/" + at + "_atlas_metadata.txt", "w+")
    f.write("Atlas saved = " + at + "\n")
    f.write("Shape of the grid (z, y, x) = " + str(shape) + "\n")
    f.write("Type of saved atlas = " + str(typ + 1) + "\n")
    f.write("Method of choosing parameters = " + str(method) + "\n")
    f.write("Sampling = " + str(sampling) + "\n")
    f.close()


def save_atlas(at, w):
    """ Fonction de stockage d'un atlas de paramètres pharmacocinétiques """
    """ Requière des atlas de tissus dans les bonnes arborescences """
    # at: Nom de l'atlas à sauver
    # w: Type d'atlas à sauver

    if w == 0:
        # Sauvegarde des probabilités de tissus dans l'ordre (WM, GM, CSF, Vessels) sous forme d'un tableau numpy de
        # forme [z -> [y -> [x -> [WM, GM, CSF, Vessels]]]] NON NORMALISÉ
        print("Saving non normalised tissue probability atlas in " + at + " folder...")
        to_save, sampling = get_atlas(at, w)
        method = None
        np.save(path_to_data + "/Atlas/" + at + "/" + at + "_prob_atlas.npy", to_save)
    elif w == 2:
        # Sauvegarde des probabilités de tissus dans l'ordre (WM, GM, CSF, Vessels) sous forme d'un tableau numpy de
        # forme [z -> [y -> [x -> [WM, GM, CSF, Vessels]]]] NORMALISÉ
        print("Saving normalised tissue probability atlas in " + at + " folder...")
        to_save, sampling = get_atlas(at, w)
        method = None
        np.save(path_to_data + "/Atlas/" + at + "/" + at + "_prob_atlas_norm.npy", to_save)
    elif w == 1:
        # Sauvegarde des indices du dernier axe du tableau [z -> [y -> [x -> [WM, GM, CSF, Vessels]]]] pour lesquels
        # la valeur de la probabilité est maximum
        print("Saving maximum indices atlas in " + at + " folder...")
        to_save, sampling = get_atlas(at, w)
        method = None
        np.save(path_to_data + "/Atlas/" + at + "/" + at + "_prob_atlas_ind_max.npy", to_save)
    else:
        # Sauvegarde des paramètres du modèle à 1 compartiment dans le dernier axe du tableau de forme
        # [z -> [y -> [x -> [k1, k2, alpha, delay]]]] en choisissant ces paramètres par rapport au tissu le plus
        # probable dans le tissu
        print("Saving 1-compartment parameters atlas in " + at + " folder...")
        to_save, method, sampling = get_atlas(at, w)
        np.save(path_to_data + "/Atlas/" + at + "/" + at + "_param1C_atlas_max.npy", to_save)

    # Sauvegarde des métadonnées de l'atlas
    write_log_to_file(at, method, sampling, w, to_save.shape)
    print("Saving complete !")


def menu(path):
    """ MENU """

    # Récupération de l'arborescence
    global path_to_data
    path_to_data = path

    print("Available Atlas:\n"
          "1 - MRA\n"
          "2 - TOF\n"
          "3 - UBA167\n"
          "4 - All\n"
          "0 - Exit")
    ch = int(input("Which Atlas would you like to save? "))

    while ch != 0:
        print("Type of data:\n"
              "1 - Non-normalised probability presences of tissue within each voxel\n"
              "2 - Maximum probability tissue in each voxel\n"
              "3 - Normalised probability presences of tissue within each voxel\n"
              "4 - Pharmacokinetic parameters in each voxel")
        par = int(input("Which type of Atlas would you like to save? "))

        w = int(par - 1)
        if ch == 1:
            save_atlas("MRA", w)
        elif ch == 2:
            save_atlas("TOF", w)
        elif ch == 3:
            save_atlas("UBA167", w)
        elif ch == 4:
            save_atlas("MRA", w)
            save_atlas("TOF", w)
            save_atlas("UBA167", w)
        print("Available Atlas:\n"
              "1 - MRA\n"
              "2 - TOF\n"
              "3 - UBA167\n"
              "4 - All\n"
              "0 - Exit")
        ch = int(input("Which Atlas would you like to save? "))
