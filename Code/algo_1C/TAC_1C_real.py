import numpy as np
import common.constants as c
import common.noise_generator as noise
import common.pve_generator as pve
import common.fwhm_computing as fwhm
import math as m

# Récupération de la demi-vie de l'isotope concerné
lamb = m.log(2) / c.HALF_LIFE_11C
# Variable globale du chemin vers les données
path_to_data = ""


def write_to_log_file(at, ty, amp, padding):
    """ Fonction écrivant les métadonnées de courbes réelles en les ajoutant à un fichier existant """

    # Ouverture du log en mode 'append'
    f = open(path_to_data + "/Atlas/" + at + "/" + at + "_TAC_metadata.txt", 'a')
    f.write("Type of noise = " + str(ty) + "\n")
    f.write("Noise amplitude = " + str(amp) + "\n")
    f.write("PVE padding size = " + str(padding) + "\n")
    f.close()


def load_all_frame(a):
    """ Fonction récupérant les TAC noiseless, le vecteur temps, l'atlas des indices et le sampling """

    # Récupération des courbes noiseless
    curves = np.load(path_to_data + "/Atlas/" + a + "/" + a + "_TAC_1C_noiseless_frame.npy").astype(np.float64)
    # Récupération du vecteur temps
    t = np.load(path_to_data + "/Atlas/" + a + "/" + a + "_TAC_1C_time.npy").astype(np.float64)
    # Récupération de l'atlas des indices
    ind = np.load(path_to_data + "/Atlas/" + a + "/" + a + "_prob_atlas_ind_max.npy").astype(np.int8)
    # Récupération des coordonnées des voxels non-vides !! format (z, y, x) !! --> mais en forme d'un array de 3 lignes
    # et autant de colonnes que de points
    coor = np.load(path_to_data + "/Atlas/" + a + "/" + a + "_TAC_1C_coor.npy")
    # Ouverture du fichier de métadonnées
    f = open(path_to_data + "/Atlas/" + a + "/" + a + "_atlas_metadata.txt", 'r')
    f1 = open(path_to_data + "/Atlas/" + a + "/" + a + "_TAC_metadata.txt", 'r')

    # Récupération de la valeur du sampling
    log = f.read().split("\n")
    f.close()
    log1 = log[-2][12:-1]
    log1 = log1.split(', ')
    sampling = (float(log1[0]), float(log1[1]), float(log1[2]))

    # Récupération de la longueur de frame
    log = f1.read().split("\n")
    f1.close()
    frame = float(log[0][-2:])

    return t, curves, ind, sampling, frame, coor


def load_all(a):
    """ Fonction récupérant les TAC noiseless, le vecteur temps, l'atlas des indices et le sampling """

    # Récupération des courbes noiseless
    curves = np.load(path_to_data + "/Atlas/" + a + "/" + a + "_TAC_1C_noiseless.npy").astype(np.float64)
    # Récupération du vecteur temps
    t = np.load(path_to_data + "/Atlas/" + a + "/" + a + "_TAC_1C_time.npy").astype(np.float64)
    # Récupération de l'atlas des indices
    ind = np.load(path_to_data + "/Atlas/" + a + "/" + a + "_prob_atlas_ind_max.npy").astype(np.int8)
    # Récupération des coordonnées des voxels non-vides !! format (z, y, x) !! --> mais en forme d'un array de 3 lignes
    # et autant de colonnes que de points
    coor = np.load(path_to_data + "/Atlas/" + a + "/" + a + "_TAC_1C_coor.npy")
    # Ouverture du fichier de métadonnées
    f = open(path_to_data + "/Atlas/" + a + "/" + a + "_atlas_metadata.txt", 'r')

    # Récupération de la valeur du sampling
    log = f.read().split("\n")
    f.close()
    log1 = log[-2][12:-1]
    log1 = log1.split(', ')
    sampling = (float(log1[0]), float(log1[1]), float(log1[2]))

    return t, curves, ind, sampling, coor


def save_curves_frame(at, ind, curves):
    """ Fonction stockant les courbes non placées dans l'atlas """

    coor = np.nonzero(ind)
    curves = curves[coor]
    np.save(path_to_data + "/Atlas/" + at + "/" + at + "_TAC_1C_real_frame.npy", curves)


def save_curves(at, ind, curves):
    """ Fonction stockant les courbes non placées dans l'atlas """

    coor = np.nonzero(ind)
    curves = curves[coor]
    np.save(path_to_data + "/Atlas/" + at + "/" + at + "_TAC_1C_real.npy", curves)


def compute_frame(at):
    """ Fonction stockant des TAC réelles (par ajout de bruit et de PVE) à partir de courbes noiseless """

    print("Loading noiseless curves...")
    # Récupération des atlas, du vecteur temps, du sampling et des TAC noiseless analytiques
    t_, tac_noiseless, ind, sampling, frame, coor = load_all_frame(at)
    print("Loading complete.")

    # Ajout de bruit aux courbes
    tac_noisy, noise_type, noise_amp = noise.add_noise(t_, tac_noiseless, frame)
    print("Noise added to " + at + " atlas.")

    # Ajout d'un PVE
    # Amplitude de l'effet de volume partiel
    padding = int(input("Padding size in voxel (example: 2): "))
    print("Available types of polynomials for FWHM interpolation:\n"
          "1 - Chebyshev\n"
          "2 - Legendre\n"
          "3 - Canonical")
    ty = int(input("Choose a polynomial type for interpolation: "))
    # Récupération d'un atlas de FWHM suivant différentes directions de l'espace généré en interpolant des mesures selon
    # des polynômes choisis
    fwhm_data = fwhm.fwhm_poly_interpolation(ind.shape[2], ind.shape[1], ind.shape[0], sampling, 2, ty, path_to_data)
    # Ajout du PVE
    tac_real = pve.add_pve(coor, tac_noisy, (ind.shape[0], ind.shape[1], ind.shape[2]), padding, fwhm_data, sampling)
    print("PVE added to " + at + " atlas")

    print("Saving frame-averaged curves...")
    # Écriture d'un rapport
    write_to_log_file(at, noise_type, noise_amp, padding)
    # Stockage des TAC réelles
    save_curves_frame(at, ind, tac_real)

    """real_tac = np.memmap(path_to_data + "/Atlas/" + at + "/" + at + "_TAC_1C_real_frame_complete.npy", dtype='float64',
                         mode='w+', shape=(ind.shape[0], ind.shape[1], ind.shape[2], t_.shape[0]))
    real_tac = tac_real
    real_tac.flush()
    del real_tac"""
    print("Saving complete")


def compute(at):
    """ Fonction stockant des TAC réelles (par ajout de bruit et de PVE) à partir de courbes noiseless """

    print("Loading noiseless curves...")
    # Récupération des atlas, du vecteur temps, du sampling et des TAC noiseless analytiques
    t_, tac_noiseless, ind, sampling, coor = load_all(at)
    print("Loading complete.")

    # Ajout de bruit aux courbes
    tac_noisy, noise_type, noise_amp = noise.add_noise(t_, tac_noiseless, 0)
    print("Noise added to " + at + " atlas.")

    # Ajout d'un PVE
    # Amplitude de l'effet de volume partiel
    padding = int(input("Padding size in voxel (example: 2): "))
    # Choix du type de polynômes pour interpolation des mesure de FWHM
    print("Available types of polynomials for FWHM interpolation:\n"
          "1 - Chebyshev\n"
          "2 - Legendre\n"
          "3 - Canonical")
    ty = int(input("Choose a polynomial type for interpolation: "))
    # Récupération d'un atlas de FWHM suivant différentes directions de l'espace généré en interpolant des mesures selon
    # des polynômes choisis
    fwhm_data = fwhm.fwhm_poly_interpolation(ind.shape[2], ind.shape[1], ind.shape[0], sampling, 2, ty, path_to_data)
    # Ajout du PVE
    tac_real = pve.add_pve(coor, tac_noisy, (ind.shape[0], ind.shape[1], ind.shape[2]), padding, fwhm_data, sampling)
    print("PVE added to " + at + " atlas")

    print("Saving curves...")
    # Écriture d'un rapport
    write_to_log_file(at, noise_type, noise_amp, padding)
    # Stockage des TAC réelles
    save_curves(at, ind, tac_real)

    """real_tac = np.memmap(path_to_data + "/Atlas/" + at + "/" + at + "_TAC_1C_real_complete.npy", dtype='float64',
                         mode='w+', shape=(ind.shape[0], ind.shape[1], ind.shape[2], t_.shape[0]))
    real_tac = tac_real
    real_tac.flush()
    del real_tac"""
    print("Saving complete")


def menu(path):
    """ MENU """

    # Récupération de l'arborescence
    global path_to_data
    path_to_data = path

    print("Available curves:\n"
          "1 - MRA\n"
          "2 - TOF\n"
          "3 - UBA167\n"
          "4 - All\n"
          "0 - Exit")
    ch = int(input("For which atlas would you like to generate real TACs? "))

    while ch != 0:
        fr = input("Create realistic curves based on frame-averaged curves? (n/y) ")
        if fr == "y":
            if ch == 1:
                compute_frame("MRA")
            elif ch == 2:
                compute_frame("TOF")
            elif ch == 3:
                compute_frame("UBA167")
            else:
                compute_frame("MRA")
                compute_frame("TOF")
                compute_frame("UBA167")
        else:
            if ch == 1:
                compute("MRA")
            elif ch == 2:
                compute("TOF")
            elif ch == 3:
                compute("UBA167")
            else:
                compute("MRA")
                compute("TOF")
                compute("UBA167")
        print("Available curves:\n"
              "1 - MRA\n"
              "2 - TOF\n"
              "3 - UBA167\n"
              "4 - All\n"
              "0 - Exit")
        ch = int(input("For which atlas would you like to generate real TACs? "))
