import numpy as np


def residual(a, x, b):
    t1 = np.matmul(a, x)
    t2 = b - t1
    num = np.linalg.norm(t2)
    denom = np.linalg.norm(b)
    return num / denom
