import numpy as np
import SimpleITK as sitk


def get_metadata(file):
    """ Fonction récupérant les métadonnées d'une image SITK et les affichant """

    reader = sitk.ImageFileReader()
    reader.SetImageIO("MetaImageIO")
    reader.SetFileName(file)
    reader.LoadPrivateTagsOn()

    reader.ReadImageInformation()

    for k in reader.GetMetaDataKeys():
        v = reader.GetMetaData(k)
        print("({0}) = = \"{1}\"".format(k, v))

    print("Image Size: {0}".format(reader.GetSize()))
    print("Image PixelType: {0}"
          .format(sitk.GetPixelIDValueAsString(reader.GetPixelID())))


"""gm_mean = np.load("/media/strangesyd/Seagate/Stage/patients/type1/gm_mean.npy")
gm_std = np.load("/media/strangesyd/Seagate/Stage/patients/type1/gm_std.npy")
wm_mean = np.load("/media/strangesyd/Seagate/Stage/patients/type1/wm_mean.npy")
wm_std = np.load("/media/strangesyd/Seagate/Stage/patients/type1/wm_std.npy")

print(gm_mean)
print(gm_std)
print(wm_mean)
print(wm_std)"""
