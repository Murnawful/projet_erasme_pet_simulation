import numpy as np
from matplotlib import pyplot as plt
import matplotlib.animation as animation


def visu_z(atlas, minimum, maximum, pause):
    """ Visualisation du tableau selon l'axe z """
    img = None
    for i in range(atlas.shape[0]):
        if img is None:
            img = plt.imshow(atlas[i, :, :], vmin=minimum, vmax=maximum, cmap='gray')
        else:
            img.set_data(atlas[i, :, :])
        plt.pause(pause)
        plt.draw()

    return 0


def visu_y(atlas, minimum, maximum, pause):
    """ Visualisation du tableau selon l'axe z """
    img = None
    for i in range(atlas.shape[1]):
        if img is None:
            img = plt.imshow(atlas[:, i, :], vmin=minimum, vmax=maximum, cmap='gray')
        else:
            img.set_data(atlas[:, i, :])
        plt.pause(pause)
        plt.draw()

    return 0


def visu_x(atlas, minimum, maximum, pause):
    """ Visualisation du tableau selon l'axe z """
    img = None
    for i in range(atlas.shape[2]):
        if img is None:
            img = plt.imshow(atlas[:, :, i], vmin=minimum, vmax=maximum, cmap='gray')
        else:
            img.set_data(atlas[:, :, i])
        plt.pause(pause)
        plt.draw()

    return 0


def visu_x_save(atlas, minimum, maximum, fps, size, name):
    n_seconds = atlas.shape[2] / fps

    # First set up the figure, the axis, and the plot element we want to animate
    fig = plt.figure(figsize=(size / atlas.shape[1], size / atlas.shape[0]))
    im = plt.imshow(atlas[:, :, 0], interpolation='none', aspect='auto', vmin=minimum, vmax=maximum, cmap='gray')

    def animate_func(i):
        if i % fps == 0:
            print('.', end='')

        im.set_array(atlas[:, :, i])
        return [im]

    anim = animation.FuncAnimation(fig, animate_func, frames=int(n_seconds * fps), interval=1000 / fps)

    anim.save(name + '_in_x.mp4', fps=fps, extra_args=['-vcodec', 'libx264'])
    del anim, fig, im


def visu_y_save(atlas, minimum, maximum, fps, size, name):
    n_seconds = atlas.shape[1] / fps

    # First set up the figure, the axis, and the plot element we want to animate
    fig = plt.figure(figsize=(size / atlas.shape[2], size / atlas.shape[0]))
    im = plt.imshow(atlas[:, 0, :], interpolation='none', aspect='auto', vmin=minimum, vmax=maximum, cmap='gray')

    def animate_func(i):
        if i % fps == 0:
            print('.', end='')

        im.set_array(atlas[:, i, :])
        return [im]

    anim = animation.FuncAnimation(fig, animate_func, frames=int(n_seconds * fps), interval=1000 / fps)

    anim.save(name + '_in_y.mp4', fps=fps, extra_args=['-vcodec', 'libx264'])
    del anim, fig, im


def visu_z_save(atlas, minimum, maximum, fps, size, name):
    n_seconds = atlas.shape[0] / fps

    # First set up the figure, the axis, and the plot element we want to animate
    fig = plt.figure(figsize=(size / atlas.shape[2], size / atlas.shape[1]))
    im = plt.imshow(atlas[0, :, :], interpolation='none', aspect='auto', vmin=minimum, vmax=maximum, cmap='gray')

    def animate_func(i):
        if i % fps == 0:
            print('.', end='')

        im.set_array(atlas[i, :, :])
        return [im]

    anim = animation.FuncAnimation(fig, animate_func, frames=int(n_seconds * fps), interval=1000 / fps)

    anim.save(name + '_in_z.mp4', fps=fps, extra_args=['-vcodec', 'libx264'])
    del anim, fig, im
