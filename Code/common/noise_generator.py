import numpy as np
import math as m
import common.constants as c


lamb = m.log(2) / c.HALF_LIFE_11C


def gamma_noise_generator(mu, t, c, dt, l):
    return 0


def lognormal_noise_generator(y, t, c, dt, l):
    """ Fonction retournant une courbe bruitée selon un modèle de bruit log normal """
    # y = courbe non bruitée (modèle du premier ou deuxième ordre)
    # t = ts + te/2 (en vecteur)
    # c = constante pour scaler le niveau de bruit (1 - 1.5)
    # dt = largeur de frame (te-ts)
    # l = constante de désintégration (import from file "constants")

    ex = np.exp(-l * t)
    s = (y * ex) / dt
    s[np.where(s < 0)] = 0
    n = c * np.sqrt(s) * np.random.lognormal(0.0, 1.0, y.shape) / ex  # formule du
    # bruit (voir article) (moyenne, écart-type, data)

    n[np.where(n + y < 0)] = 0  # tronquage pour empêcher une activité nulle ou négative

    return n  # renvoie un vecteur de bruit à additioner au signal y de sortie


def gaussian_noise_generate(y, t, c, dt, l):
    """ Fonction retournant une courbe bruitée selon un modèle de bruit normal """
    # y = courbe non bruitée (modèle du premier ou deuxième ordre)
    # t = ts + te/2 (en vecteur)
    # c = constante pour scaler le niveau de bruit (1 - 8)
    # dt = largeur de frame (te-ts)
    # l = constante de désintégration (import from file "constants")

    ex = np.exp(-l * t)
    s = (y * ex) / dt
    s[np.where(s < 0)] = 0
    n = c * np.sqrt(s) * np.random.normal(0.0, 1.0, y.shape) / ex  # formule du
    # bruit (voir article) (moyenne, écart-type, data)

    n = np.maximum(n, -y)  # tronquage pour empêcher une activité nulle ou négative

    return n  # renvoie un vecteur de bruit à additioner au signal y de sortie


def add_noise(t, tac, frame):
    print("Noise distribution:\n"
          "1 - Normal\n"
          "2 - Log Normal")
    ty = int(input("Choose a type of noise distribution: "))
    amp = float(input("Choose a noise amplitude [0.25, 8]: "))
    print("Adding noise...")
    if ty == 1:
        n = gaussian_noise_generate(tac, t, amp, frame, lamb)
    else:
        n = lognormal_noise_generator(tac, t, amp, frame, lamb)
    tac = np.add(tac, n).astype(np.float32)
    return tac, ty, amp
