import numpy as np

""" Constantes utiles pour l'algorithme """

EPS = 1.0e-16  # précision machine
HALF_LIFE_11C = 1221.780029  # 1/2 vie C11 en secondes (fixée par Philips)
LAMBDA_11C = np.log(2.0)/HALF_LIFE_11C  # constante de désintégration de C11
