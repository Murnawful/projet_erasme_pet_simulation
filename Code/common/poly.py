def chebyshev(n, x):
    if n == 0: return 1.0
    elif n == 1: return x
    else: return 2 * x * chebyshev(n - 1, x) - chebyshev(n - 2, x)


def chebyshev3D(nx, ny, nz, x, y, z):
    return chebyshev(nx, x) * chebyshev(ny, y) * chebyshev(nz, z)


def legendre(n, x):
    if n == 0: return 1.0
    elif n == 1: return x
    else: return ((2 * n + 1) * x * legendre(n - 1, x) - n * legendre(n - 2, x)) / (n + 1)


def legendre3D(nx, ny, nz, x, y, z):
    return legendre(nx, x) * legendre(ny, y) * legendre(nz, z)


def canonical(n, x):
    if n == 0: return 1.0
    else: return x * canonical(n - 1, x)


def canonical3D(nx, ny, nz, x, y, z):
    return canonical(nx, x) * canonical(ny, y) * canonical(nz, z)
