import numpy as np
from matplotlib import pyplot as plt
import common.visu as v
import SimpleITK as sitk

"""
reader = sitk.ImageFileReader()
reader.SetFileName("/media/strangesyd/Seagate/Stage/06/Brain.mha")
reader.ReadImageInformation()

# pet = np.memmap("/media/strangesyd/Seagate/Stage/06/temp.npy", dtype='float64', mode='w+', shape=reader.GetSize())

pet = sitk.GetArrayFromImage(sitk.ReadImage("/media/strangesyd/Seagate/Stage/06/Tumour.mha"))

v.visu_y(pet, 0, 2, .05)

del pet
"""
"""
coor = np.load("/media/strangesyd/Seagate/Stage/Atlas/MRA/MRA_TAC_1C_coor.npy")
tac_noiseless = np.load("/media/strangesyd/Seagate/Stage/Atlas/MRA/MRA_TAC_1C_noiseless.npy")
atlas = np.load("/media/strangesyd/Seagate/Stage/Atlas/MRA/MRA_param1C_atlas_max.npy")

print(coor)
print(tac_noiseless[0, :])
print(atlas[coor[0][0], coor[1][0], coor[2][0]])
"""

at = "TOF"
ty = "real"
frame = "_frame"

tac_real = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_TAC_1C_real" + frame + ".npy")
tac_noiseless = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_TAC_1C_noiseless" + frame + ".npy")

t_ = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_TAC_1C_time.npy")
coor = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_TAC_1C_coor.npy")
ind = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_prob_atlas_ind_max.npy")
atlas = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_param1C_atlas_max.npy")

np.random.seed(75746)
num = np.random.randint(0, tac_real.shape[0])

temp_real = np.zeros(ind.shape)

# z, y, x = coor[0][num], coor[1][num], coor[2][num]
z, y, x = 48, 56, 55
for i in range(len(coor[0][:])):
    temp_real[coor[0][i], coor[1][i], coor[2][i]] = tac_real[i, 900]

for i in range(len(coor[0])):
    if coor[0][i] == z and coor[1][i] == y and coor[2][i] == x:
        num = i

m = 10**(-4)

print(atlas.shape)
print(atlas[48, 50, 50, 2])

plt.rcParams["figure.figsize"] = (10, 20)
plt.rcParams.update({'font.size': 16})

# temp_real[48, :, :]

plt.imshow(atlas[48, :, :, 1])
plt.xlabel("x [voxels]")
plt.ylabel("y [voxels]")

cbar = plt.colorbar(label='$k_2$ [min$^{-1}$]')
# ['Empty', 'White Matter', 'Gray Matter', 'CSF', 'Vessels']
"""
plt.plot(t_, tac_real[num])
plt.grid()
plt.xlabel("Time [s]")
plt.ylabel("Activity [Bq]")

plt.vlines(900, 0, 1200, linestyles='--')
"""

plt.show()

# v.visu_z(temp_real, 0, me, .1)

# v.visu_z_save(temp_real, 0, me, 10, 1000, "MRA_real")
