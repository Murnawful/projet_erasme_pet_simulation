import numpy as np
import common.poly as p


def compute_poly_3D(coord, coef, degre, typ, coor_size):
    """ Fonction renvoyant un atlas de valeurs de FWHM interpolée par des polynômes à partir de mesures """
    # coord: array 4D de coordonnées en mm des voxels par rapport au centre du repère
    # coef: array contenant les coefficient des polynômes
    # degree: degré max du polynôme
    # ty: type de polynôme d'interpolation

    # Initialisation de l'atlas à des valeurs nulles
    res = np.zeros(coord.shape[0:3])
    # Séparation des valeurs en millimètres selon chaque axe
    coor_x, coor_y, coor_z = coord[:, :, :, 0], coord[:, :, :, 1], coord[:, :, :, 2]

    # Remplissage de l'atlas en utilisant les polynômes pondérés par les coefficients calculés précédemment
    n = 0
    for nx in range(0, degre + 1, 2):
        for ny in range(0, degre + 1 - nx, 2):
            for nz in range(0, degre + 1 - nx - ny, 2):
                if typ == 'Chebyshev':
                    res += coef[n] * p.chebyshev3D(nx, ny, nz, coor_x / coor_size[0], coor_y / coor_size[1],
                                                   coor_z / coor_size[2])
                    n += 1
                elif typ == 'Legendre':
                    res += coef[n] * p.legendre3D(nx, ny, nz, coor_x / coor_size[0], coor_y / coor_size[1],
                                                  coor_z / coor_size[2])
                    n += 1
                else:
                    res += coef[n] * p.canonical3D(nx, ny, nz, coor_x / coor_size[0], coor_y / coor_size[1],
                                                   coor_z / coor_size[2])
                    n += 1

    return res


def get_size_mm(data):
    """ Fonction récupérant un dictionnaire dont les keys sont des tuples de coordonnées et renvoie l'amplitude maximum
    de ces coordonnées """

    # Obligatoirement fixées par le constructeur !!

    x_c, y_c, z_c = 338, 338, 82

    return x_c, y_c, z_c


def poly_coef(data_c, polynomial_degree, polynomial_type, coor_size):
    """ Fonction calculant les coefficients de polynômes pour l'interpolation en résolvant un système du type Ax = b """

    # Comptage du nombre de polynômes
    number_of_polynomials = 0
    for nx in range(0, polynomial_degree + 1, 2):
        for ny in range(0, polynomial_degree + 1 - nx, 2):
            for nz in range(0, polynomial_degree + 1 - nx - ny, 2):
                number_of_polynomials += 1

    # Génération des polynômes sous forme de matrice A
    # ATTENTION: Normalisation pour ramener le domaine des polynômes à [-1, 1]
    polynomial_matrix = np.zeros((len(data_c), number_of_polynomials))
    m = 0
    for (x, y, z) in data_c.keys():
        n = 0
        for nx in range(0, polynomial_degree + 1, 2):
            for ny in range(0, polynomial_degree + 1 - nx, 2):
                for nz in range(0, polynomial_degree + 1 - nx - ny, 2):
                    if polynomial_type == 'Chebyshev':
                        polynomial_matrix[m, n] = p.chebyshev3D(nx, ny, nz, x / coor_size[0], y / coor_size[1],
                                                                z / coor_size[0])
                    elif polynomial_type == 'Legendre':
                        polynomial_matrix[m, n] = p.legendre3D(nx, ny, nz, x / coor_size[0], y / coor_size[1],
                                                               z / coor_size[0])
                    else:
                        polynomial_matrix[m, n] = p.canonical3D(nx, ny, nz, x / coor_size[0], y / coor_size[1],
                                                                z / coor_size[0])
                    n += 1
        m += 1

    # Création du vecteur b
    fwhm_data = np.array(list(data_c.values()))

    # Résolution par les moindres carrés d'un système Ax = b mal déterminé
    sol = np.linalg.lstsq(polynomial_matrix, fwhm_data, rcond=-1)[0]

    return sol


def unpack_data(file_res, file_origin):
    """ Fonction récupérant et traitant les données issues de mesures et l'origine du volume actif """

    # Ouverture des fichiers .csv
    f1 = open(file_res, 'r')
    f2 = open(file_origin, 'r')

    s1 = f1.read().split("\n")
    s2 = f2.read().split("\n")

    f1.close()
    f2.close()

    # La dernière ligne est toujours vide
    d = s1[1:len(s1) - 1]

    # Conversion en array 1D des coordonnées de l'origine en millimètres du volume actif
    origin_data = s2[-2].split(',')
    origin_data = list(filter(lambda a: a != '', origin_data))
    origin_data = np.array([float(i) for i in origin_data])

    # Stockage dans un dictionnaire des mesures
    all_data = {}
    for i in range(len(d)):
        d[i] = d[i].split(",")
        for j in range(len(d[i])):
            # Si il n'y a pas de mesure pour ce point, la valeur est None
            if d[i][j] == '':
                d[i][j] = None
            # Si il y a une mesure, on la converti en float
            else:
                d[i][j] = float(d[i][j])
        # Remplissage du dictionnaire avec en keys les coordonnées en millimètres des points de mesure et en values les
        # données des mesures
        all_data[(d[i][0], d[i][1], d[i][2])] = np.array(d[i][3:])
        # Ajout de points de mesure symétriques pour tenir compte de la symétrie du scanner
        all_data[(-d[i][0], -d[i][1], -d[i][2])] = np.array(d[i][3:])

    # Séparation des données selon les axes et élimination des None
    all_data_x, all_data_y, all_data_z = {}, {}, {}
    for (x, y, z), (in_x, in_y, in_z) in all_data.items():
        if in_x is not None:
            all_data_x[(x, y, z)] = in_x
        if in_y is not None:
            all_data_y[(x, y, z)] = in_y
        if in_z is not None:
            all_data_z[(x, y, z)] = in_z

    return all_data, all_data_x, all_data_y, all_data_z, origin_data


def array_building(v_x, v_y, v_z, origin, s):
    """ Fonction construisant un atlas contenant les distances en centre de cet atlas en millimètres """

    # Création des vecteurs contenant le sampling en millimètres
    sampling_x = np.linspace(origin[0], origin[0] + v_x * s[0], num=v_x)
    sampling_y = np.linspace(origin[1], origin[1] + v_y * s[1], num=v_y)
    sampling_z = np.linspace(origin[2], origin[2] + v_z * s[2], num=v_z)

    # Création des atlas replaçant ces données dans un array 3D
    mm_x = np.expand_dims(np.tile(np.tile(sampling_x, (len(sampling_y), 1)), (len(sampling_z), 1, 1)), axis=-1)
    temp_y = sampling_y[:, np.newaxis]
    temp_z = sampling_z[:, np.newaxis, np.newaxis]
    mm_y = np.expand_dims(np.tile(np.tile(temp_y, (1, len(sampling_x))), (len(sampling_z), 1, 1)), axis=-1)
    mm_z = np.expand_dims(np.tile(np.tile(temp_z, (1, len(sampling_y), 1)), (1, 1, len(sampling_x))), axis=-1)

    # Création de l'array contenant pour chaque voxel ses coordonnées relatives au centre du scanner en millimètres
    sampling_mm = np.concatenate((mm_x, mm_y, mm_z), axis=-1)

    return sampling_mm


def fwhm_poly_interpolation(v_x, v_y, v_z, sampling, degree, ty, path):
    """ Fonction réalisant une interpolation polynômiale de type donné de degré donné des FWHM selon chaque axe sur un
    tableau de  forme (v_z, v_y, v_x) """

    print("Interpolating FWHM data...")
    # Récupération des données de FWHM sous forme de dictionnaires, un pour chaque FWHM
    data, data_x, data_y, data_z, origin_mm = unpack_data(path + "/Resolution/reso.csv", path +
                                                          "/Resolution/origin.csv")

    # Construction d'un atlas contenant les distances au centre du scanner des centres des voxels
    array_mm = array_building(v_x, v_y, v_z, origin_mm, sampling)
    # Conversion du type de polynôme
    if ty == 1:
        ty = "Chebyshev"
    elif ty == 2:
        ty = "Legendre"
    else:
        ty = "Canonical"
    print(ty)
    coor_size = get_size_mm(data)

    # Calcul des coefficients des polynômes à calculer sur les grilles
    coef_x = poly_coef(data_x, degree, ty, coor_size)
    coef_y = poly_coef(data_y, degree, ty, coor_size)
    coef_z = poly_coef(data_z, degree, ty, coor_size)

    # Récupération des valeurs de FWHM selon chaque axe par interpolation polynômiale
    fwhm_x = compute_poly_3D(array_mm, coef_x, degree, ty, coor_size)
    fwhm_y = compute_poly_3D(array_mm, coef_y, degree, ty, coor_size)
    fwhm_z = compute_poly_3D(array_mm, coef_z, degree, ty, coor_size)
    print("Interpolation complete.")

    return fwhm_x, fwhm_y, fwhm_z


"""
array_building(95, 117, 99, (-94.0, -56.0, -49.0), (2.0, 2.0, 2.0))
# Test du kernel 3D

import scipy.signal as signal


def kernel3d(radius, fwhm, spacing):
    num = 2 * radius + 1

    std_x = (fwhm[0] / (2 * me.sqrt(2 * me.log(2)))) / spacing[0]
    std_y = (fwhm[1] / (2 * me.sqrt(2 * me.log(2)))) / spacing[1]
    std_z = (fwhm[2] / (2 * me.sqrt(2 * me.log(2)))) / spacing[2]

    kernel_1d_x = signal.gaussian(num, std=std_x)
    kernel_1d_y = signal.gaussian(num, std=std_y)
    kernel_1d_z = signal.gaussian(num, std=std_z)
    kernel_2d = np.outer(kernel_1d_y, kernel_1d_x)
    kernel_3d = np.outer(kernel_2d, kernel_1d_z).reshape(num, num, num)
    kernel_3d = np.expand_dims(kernel_3d, -1)
    return kernel_3d


f_x, f_y, f_z = fwhm_poly_interpolation(338, 338, 82, (2, 2, 2), 4, 3, "/media/strangesyd/Seagate/Stage")

print(np.amin(f_x))
print(np.amax(f_x))
print(f_x.shape)


ker = kernel3d(100, (f_x[50, 50, 50], f_y[50, 50, 50], f_z[50, 50, 50]), (2, 2, 2))

plt.imshow(f_x[0, :, :])
plt.show()

# v.visu_z(f_x, np.amin(f_x), np.amax(f_x), .05)
# v.visu_z(f_y, np.amin(f_y), np.amax(f_y), .005)
# v.visu_z(f_z, np.amin(f_z), np.amax(f_z), .005)
"""

"""
v.visu_x_save(f_z, 0, 10, 10, "legendre")
v.visu_x_save(f_z1, 0, 10, 10, "chebyshev")
v.visu_x_save(f_z2, 0, 10, 10, "kebab")
v.visu_y_save(f_z, 0, 10, 10, "legendre")
v.visu_y_save(f_z1, 0, 10, 10, "chebyshev")
v.visu_y_save(f_z2, 0, 10, 10, "kebab")
v.visu_z_save(f_z, 0, 10, 10, "legendre")
v.visu_z_save(f_z1, 0, 10, 10, "chebyshev")
v.visu_z_save(f_z2, 0, 10, 10, "kebab")
"""
