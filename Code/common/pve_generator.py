from scipy import signal
import numpy as np
import math as m


def kernel3d(radius, fwhm_x, fwhm_y, fwhm_z, spacing):
    """ Fonction retournant un kernel en 3D de différentes FWHM selon les 3 directions de l'espace """

    num = 2 * radius + 1

    std_x = (fwhm_x / (2 * m.sqrt(2 * m.log(2)))) / spacing[0]
    std_y = (fwhm_y / (2 * m.sqrt(2 * m.log(2)))) / spacing[1]
    std_z = (fwhm_z / (2 * m.sqrt(2 * m.log(2)))) / spacing[2]

    kernel_1d_x = signal.gaussian(num, std=std_x)
    kernel_1d_y = signal.gaussian(num, std=std_y)
    kernel_1d_z = signal.gaussian(num, std=std_z)
    kernel_2d = np.outer(kernel_1d_y, kernel_1d_x)
    kernel_3d = np.outer(kernel_2d, kernel_1d_z).reshape(num, num, num)
    kernel_3d = np.expand_dims(kernel_3d, -1)
    return kernel_3d


def add_pve(coor, curves, sh, m, fwhm, spacing):
    """ Fonction recevant des courbes sans PVE et leurs coordonnées pour leur ajouter du PVE avec un kernel gaussien de
    FWHM donnée dans chaque direction pour chaque voxel """
    # coor: coordonnées des voxels non-vides
    # curves: les courbes auxquelles ajouter du PVE
    # ind: tableau des indices de tissus
    # me: largeur de padding, amplitude en voxels du kernel
    # fwhm: tuple contenant les arrays des FWHM dans chaque direction de l'espace, pour chaque voxel
    # spacing: tuple contenant l'écart entre le centre de chaque voxel selon les 3 directions de l'espace

    # Récupération des longueur des vecteurs
    print("Adding PVE to curves...")
    nx, ny, nz, nt, nnz = sh[2], sh[1], sh[0], curves.shape[-1], curves.shape[0]
    # Création d'un dictionnaire pour regrouper les courbes et leurs coordonnées dans la grille
    tseries = {}

    # Remplissage du tableau: en keys les coordonnées de cette courbe dans la grille, en values les données temporelles
    # de cette courbe
    for i in range(nnz):
        # Parcours de chacune des courbes
        while True:
            # Récupère les coordonnées de la courbe !! la conversion au format (x, y, z) se fait ici !!
            ijk = (coor[2, i], coor[1, i], coor[0, i])
            if ijk not in tseries:
                # Associe une courbe à ses coordonnées
                tseries[ijk] = curves[i, :]
                break

    cag_4dp = np.zeros((nx + 2 * m, ny + 2 * m, nz + 2 * m, nt))

    mm = 2 * m + 1  # kernel width
    for (i, j, k), tdata in tseries.items():
        kernel = kernel3d(m, fwhm[0][k, j, i], fwhm[1][k, j, i], fwhm[2][k, j, i], spacing)
        # convolution of one voxel by kernel is trivial.
        # slice4d_c has shape (mm, mm, mm, nt).
        slice4d_c = kernel * tdata

        cag_4dp[k:k + mm, j:j + mm, i:i + mm, :] += slice4d_c

    cag_4d = cag_4dp[m:-m, m:-m, m:-m, :]
    return cag_4d
