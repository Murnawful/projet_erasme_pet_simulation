import numpy as np
from scipy.signal import lsim, lti


def first_order_model_generate(x, t, gain, tau, delay, alpha, hct=0.45):
    # Modèle 1 compartiment avec paramètres modifiés
    # x = fonction d'entrée (de Feng) (peut être pris de /06/Input Function.npy
    # gain = K1/K2
    # tau = 1/K2
    # alpha = fraction vasculaire (pourcentage de vaisseaux sanguins dans le voxel)


    # system = lti([alpha*(1.0-hct)*tau, alpha*(1.0-hct)+(1.0-alpha)*gain], [(1.0-hct)*tau, (1.0-hct)])
    # _, y, _ = lsim(system,  x, t)
    # y = np.interp(t-delay, t, y)

    system = lti([gain], [tau, 1.0]) # système linéaire permanent: G/(tau*p + 1)
    _, y, _ = lsim(system,  x, t) # prend système qui simule la réponse d'un système à une entrée (le t doit avoir un timestep constant)
    y = alpha*x + (1.0-alpha)/(1.0-hct)*y # réponse avec correction hématocrite pour tenir compte de la séparation plasma-globules (on a besoin du plasmatique)
    y = np.interp(t-delay, t, y) # pour tenir compte du délai entre la carotide et le cerveau

    return y