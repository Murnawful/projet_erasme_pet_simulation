import numpy as np


def feng_input_function_generate(t, a1, a2, a3, l1, l2, l3, d):
    # Modèle impulsionnel sanguin (sans correction plasma -> whole blood) de Feng (signal d'entrée)

    t_ = t-d # vecteur temps moins le délai

    x = (a1*t_-a2-a3)*np.exp(l1*t_) + a2*np.exp(l2*t_) + a3*np.exp(l3*t_) # équation du modèle de Feng
    x[np.where(t_ < 0.0)] = 0.0 # système causal donc tout ce qui est t < 0 est =0

    return x


def feng_input_function_generate_frame(ts, te, a1, a2, a3, l1, l2, l3, d):
    # Intégrale de la fonction précédente entre start et end

    ts_ = np.maximum(ts-d, 0.0)
    te_ = np.maximum(te-d, 0.0)

    x = ((a1*(te_-1.0/l1)-a2-a3)/l1*np.exp(l1*te_) - (a1*(ts_-1.0/l1)-a2-a3)/l1*np.exp(l1*ts_) + a2/l2*(np.exp(l2*te_)-np.exp(l2*ts_)) + a3/l3*(np.exp(l3*te_)-np.exp(l3*ts_)))/(te-ts) # formule de l'intégrale

    return x