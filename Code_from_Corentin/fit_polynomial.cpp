double chebyshev(int n, double x) {
	return (n == 0) ? 1.0 : ((n == 1) ? x : (2 * x * chebyshev(n - 1, x) - chebyshev(n - 2, x)));
}

double chebyshev3D(int nx, int ny, int nz, double x, double y, double z) {
	return chebyshev(nx, x) * chebyshev(ny, y) * chebyshev(nz, z);
}

double legendre(int n, double x) {
	return (n == 0) ? 1.0 : ((n == 1) ? x : ((2 * n + 1) * x * legendre(n - 1, x) - n * legendre(n - 2, x)) / (n + 1));
}

double legendre3D(int nx, int ny, int nz, double x, double y, double z) {
	return legendre(nx, x) * legendre(ny, y) * legendre(nz, z);
}

/*
double standard(int n, double x) {
	double res = 1.0;
	for (int i = 0; i != n; i++) {
		res *= x;
	}
	return res;
}
*/

double canonical(int n, double x) {
    return (n==0) ? 1.0 : x * canonical(n - 1, x);
}

double canonical3D(int nx, int ny, int nz, double x, double y, double z) {
    return canonical(nx, x) * canonical(ny, y) * canonical(nz, z);
}

Index m = 0;

for (int z = bounds[4]; z <= bounds[5]; z++) {
	for (int y = bounds[2]; y <= bounds[3]; y++) {
		for (int x = bounds[0]; x <= bounds[1]; x++) { 
		               
			int index[3] = {x, y, z};
			double coordinates[3];
			
			for (int i = 0; i != 3; i++)
				coordinates[i] = (index[i] * spacing[i] - center[i]) / maxHalfExtent;
			
			Index n = 0;      
			              
			for (int nx = 0; nx != this->polynomialDegree + 1; nx++) {
				for (int ny = 0; ny != this->polynomialDegree + 1 - nx; ny++) {
					for (int nz = 0; nz != this->polynomialDegree + 1 - nx - ny; nz++) {
					
						switch (this->polynomialType) {
						    case CHEBYSHEV:
							polynomialMatrix(m, n) = chebyshev3D(nx, ny, nz, coordinates[0], coordinates[1], coordinates[2]);
							break;
						    case LEGENDRE:
							polynomialMatrix(m, n) = legendre3D(nx, ny, nz, coordinates[0], coordinates[1], coordinates[2]);
							break;
						    case STANDARD:
							polynomialMatrix(m, n) = canonical3D(nx, ny, nz, coordinates[0], coordinates[1], coordinates[2]);
							break;
							
						}
					n++;
					}
				}
			}
			m++;
		}
	}
}


/* Calcule le degré du polynôme dans chaque partie */
Index numberOfPolynomials = 0;

for (int nx = 0; nx != this->polynomialDegree + 1; nx++) {
	for (int ny = 0; ny != this->polynomialDegree + 1 - nx; ny++) {
		for (int nz = 0; nz != this->polynomialDegree + 1 - nx - ny; nz++) {
		
			numberOfPolynomials++;
			
		}
	}
}
