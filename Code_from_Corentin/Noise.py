import numpy as np


def noise_generate(y, t, c, dt, l):
    # y = courbe non bruitée (modèle du premier ou deuxième ordre)
    # t = ts + te/2 (en vecteur)
    # c = constante pour scaler le niveau de bruit (1 - 1.5)
    # dt = largeur de frame (te-ts)
    # l = constante de désintégration (import from file "constants")

    n = c*np.sqrt(y/(dt*np.exp(-l*t)))*np.random.normal(0.0, 1.0, y.shape) # formule du bruit (voir article)
    n = np.maximum(n, -y) # tronquage pour empêcher une activité nulle ou négative

    return n # renvoie un vecteur de bruit à additioner au signal y de sortie
