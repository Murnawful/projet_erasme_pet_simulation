import numpy as np
from FengInputFunction import feng_input_function_generate, feng_input_function_generate_frame
from scipy.signal import lsim, lti


def one_tissue_compartmental_model_generate(x, t, k1, k2, alpha, delay, hct=0.45):
    # exactement pareil que dans le modèle précédent mais avec les paramètres k1, k2

    # system = lti([alpha*(1.0-hct), alpha*(1.0-hct)*k2 + (1.0-alpha)*k1], [(1.0-hct), (1.0-hct)*k2])
    # _, y, _ = lsim(system,  x, t)
    # y = np.interp(t-delay, t, y)

    system = lti([k1], [1.0, k2])
    _, y, _ = lsim(system,  x, t)
    y = alpha*x + (1.0-alpha)/(1.0-hct)*y
    y = np.interp(t-delay, t, y)

    return y


def one_tissue_compartmental_model_generate_analytical(t, a1, a2, a3, l1, l2, l3, d, k1, k2, alpha, delay, hct=0.45):
    # version analytique du modèle à un compartiment (à utiliser avec une entrée de Feng et donc les paramètres de Feng)
    # d = délai de Feng
    # delay = délai entre Feng et la sortie

    t_ = t-(d+delay) # vecteur temps (avec les 2 délais)

    l = [l1, l2, l3] # changement de variable (voir article)
    a = [-a2-a3-a1/(k1+l1), a2, a3] # a' dans l'article

    y = (a1*t_*k1)/(k2+l1)*np.exp(l1*t_)

    for i in range(3):

        y += a[i]*k1/(k2+l[i])*(np.exp(l[i]*t_)-np.exp(-k2*t_))

    y[np.where(t_ < 0.0)] = 0.0
    y = alpha*feng_input_function_generate(t, a1, a2, a3, l1, l2, l3, d+delay) + (1.0-alpha)/(1.0-hct)*y # correction plasmatique [alpha*(le sang) + (1-alpha)*(le plasma)] (moyenne de Feng)

    return y


def one_tissue_compartmental_model_generate_analytical_frame(ts, te, a1, a2, a3, l1, l2, l3, d, k1, k2, alpha, delay, hct=0.45):
    # même chose mais activité moyenne dans une frame qui commence à ts et finit à te (solution analytique)

    ts_ = np.maximum(ts-(d+delay), 0.0)
    te_ = np.maximum(te-(d+delay), 0.0)

    l = [l1, l2, l3]
    a = [-a2-a3-a1/(k1+l1), a2, a3]

    y = a1*k1*(np.exp(l1*ts_)*(1.0-l1*ts_)-np.exp(l1*te_)*(1.0-l1*te_))/(l1**2.0*(k2+l1))

    for j in range(3):

        y += a[j]*k1/(k2+l[j])*((np.exp(-k2*te_)-np.exp(-k2*ts_))/k2 + (np.exp(l[j]*te_)-np.exp(l[j]*ts_))/l[j])

    y /= (te-ts)
    y = alpha*feng_input_function_generate_frame(ts, te, a1, a2, a3, l1, l2, l3, d+delay) + (1.0-alpha)/(1.0-hct)*y # moyenne de Feng

    return y