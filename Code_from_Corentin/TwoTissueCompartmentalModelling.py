import numpy as np
from FengInputFunction import feng_input_function_generate, feng_input_function_generate_frame
from scipy.signal import lsim, lti

# Même chose que dans le fichier précédent mais à 2 compartiments

def two_tissue_compartmental_model_generate(x, t, k1, k2, k3, k4, alpha, delay, hct=0.45):

    # system = lti([alpha*(1.0-hct), alpha*(1.0-hct)*(k2+k3+k4) + (1.0-alpha)*k1, alpha*(1.0-hct)*k2*k4 + (1.0-alpha)*k1*(k3+k4)], [(1.0-hct), (1.0-hct)*(k2+k3+k4), (1.0-hct)*k2*k4])
    # _, y, _ = lsim(system,  x, t)
    # y = np.interp(t-delay, t, y)

    system = lti([k1, k1*(k3+k4)], [1.0, k2+k3+k4, k2*k4])
    _, y, _ = lsim(system,  x, t)
    y = alpha*x + (1.0-alpha)/(1.0-hct)*y
    y = np.interp(t-delay, t, y)

    return y


def two_tissue_compartmental_model_generate_analytical(t, a1, a2, a3, l1, l2, l3, d, k1, k2, k3, k4, alpha, delay, hct=0.45):

    t_ = t-(d+delay)

    l = [l1, l2, l3]
    q = np.sqrt((k2+k3+k4)**2.0-4.0*k2*k4)
    m = [0.5*(k2+k3+k4-q), 0.5*(k2+k3+k4+q)]
    b = [k1/(m[1]-m[0])*(k3+k4-m[0]), k1/(m[1]-m[0])*(m[1]-k3-k4)]
    a = [[-a2-a3-a1/(b[0]+l1), a2, a3],
         [-a2-a3-a1/(b[1]+l1), a2, a3]]

    y = np.zeros_like(t)

    for i in range(2):

        y += (a1*t_*b[i])/(m[i]+l1)*np.exp(l1*t_)

        for j in range(3):

            y += a[i][j]*b[i]/(m[i]+l[j])*(np.exp(l[j]*t_)-np.exp(-m[i]*t_))

    y[np.where(t_ < 0.0)] = 0.0
    y = alpha*feng_input_function_generate(t, a1, a2, a3, l1, l2, l3, d+delay) + (1.0-alpha)/(1.0-hct)*y # hct = hématocrite (volume du sang occupé par les globules rouges)

    return y


def two_tissue_compartmental_model_generate_analytical_frame(ts, te, a1, a2, a3, l1, l2, l3, d, k1, k2, k3, k4, alpha, delay, hct=0.45):

    ts_ = np.maximum(ts-(d+delay), 0.0)
    te_ = np.maximum(te-(d+delay), 0.0)

    l = [l1, l2, l3]
    q = np.sqrt((k2+k3+k4)**2.0-4.0*k2*k4)
    m = [0.5*(k2+k3+k4-q), 0.5*(k2+k3+k4+q)]
    b = [k1/(m[1]-m[0])*(k3+k4-m[0]), k1/(m[1]-m[0])*(m[1]-k3-k4)]
    a = [[-a2-a3-a1/(b[0]+l1), a2, a3],
         [-a2-a3-a1/(b[1]+l1), a2, a3]]

    y = np.zeros_like(ts)

    for i in range(2):

        y += a1*b[i]*(np.exp(l1*ts_)*(1.0-l1*ts_)-np.exp(l1*te_)*(1.0-l1*te_))/(l1**2.0*(m[i]+l1))

        for j in range(3):

            y += a[i][j]*b[i]/(m[i]+l[j])*((np.exp(-m[i]*te_)-np.exp(-m[i]*ts_))/m[i] + (np.exp(l[j]*te_)-np.exp(l[j]*ts_))/l[j])

    y /= (te-ts)
    y = alpha*feng_input_function_generate_frame(ts, te, a1, a2, a3, l1, l2, l3, d+delay) + (1.0-alpha)/(1.0-hct)*y

    return y