import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import math

xlim = 7
ylim = 5

plt.figure()
ax = plt.gca()

ax.arrow(-xlim, 0, 2*xlim, 0, head_width=.07, head_length=.1, fc='k', ec='k', zorder=100, length_includes_head=True)
ax.arrow(0, -.5, 0, ylim + .5, head_width=.07, head_length=.1, fc='k', ec='k', zorder=100, length_includes_head=True)

ax.arrow(-1.455, 0, 2*1.455, 0, head_width=.07, head_length=.1, fc='r', ec='r', zorder=100, length_includes_head=True)
ax.arrow(1.3, 0, -1.3-1.455, 0, head_width=.07, head_length=.1, fc='r', ec='r', zorder=100, length_includes_head=True)

plt.annotate('FWHM', (.5, .1), color='r')

ax.set_xlim([-xlim, xlim])
ax.set_ylim([-.5, ylim])

ax.spines['top'].set_color('none')
ax.spines['bottom'].set_position('zero')
ax.spines['left'].set_position('zero')
ax.spines['right'].set_color('none')

plt.annotate('$x$', (7, 0.1))

plt.xticks([-1.455, 1.455], [])
plt.yticks([], [])
plt.tick_params(axis='both', colors='r')

mu = 0
variance = 2
sigma = math.sqrt(variance)
x = np.linspace(mu - 7*sigma, mu + 7*sigma, 1000)
plt.plot(x, 15*stats.norm.pdf(x, mu, sigma))

plt.vlines(x=1.455, ymin=0, ymax=2.5, linestyle='--', color='gray', alpha=.5)
plt.vlines(x=-1.455, ymin=0, ymax=2.5, linestyle='--', color='gray', alpha=.5)

plt.show()
