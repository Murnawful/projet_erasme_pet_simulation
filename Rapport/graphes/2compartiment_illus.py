from matplotlib import pyplot as plt

plt.axes()
rect_capi = plt.Rectangle((0, 0), width=20, height=20, angle=0, color='r', alpha=.7)
rect_comp = plt.Rectangle((30, 0), 20, 20, 0, color='b', alpha=.7)
rect_comp1 = plt.Rectangle((60, 0), 20, 20, 0, color='b', alpha=.7)
rect_comp2 = plt.Rectangle((30, -30), 20, 20, 0, color='b', alpha=.7)
plt.gca().add_patch(rect_capi)
plt.gca().add_patch(rect_comp)
plt.gca().add_patch(rect_comp1)
plt.gca().add_patch(rect_comp2)

plt.gca().arrow(21, 14, 8, 0, head_width=.5, head_length=1, fc='k', ec='k', length_includes_head=True)
plt.gca().arrow(29, 6, -8, 0, head_width=.5, head_length=1, fc='k', ec='k', length_includes_head=True)

plt.gca().arrow(51, 14, 8, 0, head_width=.5, head_length=1, fc='k', ec='k', length_includes_head=True)
plt.gca().arrow(59, 6, -8, 0, head_width=.5, head_length=1, fc='k', ec='k', length_includes_head=True)

plt.gca().arrow(36, -9, 0, 8, head_width=.5, head_length=1, fc='k', ec='k', length_includes_head=True)
plt.gca().arrow(44, -1, 0, -8, head_width=.5, head_length=1, fc='k', ec='k', length_includes_head=True)

plt.annotate("$C_p (t)$", (5.5, 8.5), color='k', size=16)
plt.annotate("$C_{T1} (t)$", (36, 8.5), color='k', size=16)
plt.annotate("$C_{T2} (t)$", (65, 8.5), color='k', size=16)
plt.annotate("$C_{T3} (t)$", (36, -21.5), color='k', size=16)

plt.annotate("$K_1$", (23, 15.5), color='k', size=15)
plt.annotate("$k_2$", (23.5, 2), color='k', size=15)

plt.annotate("$k_3$", (53, 15.5), color='k', size=15)
plt.annotate("$k_4$", (53.5, 2), color='k', size=15)

plt.annotate("$k_5$", (45, -6), color='k', size=15)
plt.annotate("$k_6$", (31, -6), color='k', size=15)

plt.axis('scaled')
plt.axis('off')

plt.show()
