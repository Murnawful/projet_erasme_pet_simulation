import matplotlib.pyplot as plt
import math as m
from matplotlib import patches

xlim = 7
ylim = 5

plt.figure()
ax = plt.gca()

ax.arrow(-.5, 0, xlim + .5, 0, head_width=.07, head_length=.1, fc='k', ec='k', zorder=100, length_includes_head=True)
ax.arrow(0, -.5, 0, ylim + .5, head_width=.07, head_length=.1, fc='k', ec='k', zorder=100, length_includes_head=True)

ax.arrow(0, 0, 5, 3, head_width=.05, head_length=.1, fc='g', ec='g', zorder=100, length_includes_head=True)
ax.arrow(5, 3, -2, 0, head_width=.05, head_length=.1, fc='g', ec='g', zorder=100, length_includes_head=True)
ax.arrow(0, 0, 3, 3, head_width=.05, head_length=.1, fc='r', ec='r', zorder=100, length_includes_head=True)

plt.annotate('$\\vec{p}_1$', (4, 2), color='g')
plt.annotate('$\\vec{p}_2$', (3.7, 3.2), color='g')
plt.annotate('$\\vec{p}$', (1.5, 1.8), color='r')

ax.set_xlim([-.5, xlim])
ax.set_ylim([-.5, ylim])

ax.spines['top'].set_color('none')
ax.spines['bottom'].set_position('zero')
ax.spines['left'].set_position('zero')
ax.spines['right'].set_color('none')

plt.plot([0, xlim], [3, 3], linestyle='--', color='gray', alpha=.5)
plt.plot([0, xlim], [0, (3/5) * xlim], linestyle='--', color='gray', alpha=.5)
plt.vlines(x=3, ymin=0, ymax=3, linestyle='--', color='gray', alpha=.5)

plt.annotate('$\\vec{u}_z$', (7, 0.1))
plt.annotate('$\\vec{u}_r$', (.1, 5))

e = patches.Arc((5, 3), 2, 2, theta1=0, theta2=m.atan(3/5) * 180 / m.pi, angle=0, linewidth=1, fill=False, zorder=2,
                color='b')
ax.add_patch(e)
plt.annotate('$\\theta_{12}$', (5.5, 3.1), color='b')

plt.xticks([3], ['$p_z$'])
plt.yticks([3], ['$p_r$'])
plt.tick_params(axis='both', colors='r')

plt.show()
