import numpy as np
import matplotlib.pyplot as plt


x = np.linspace(-1, 1, 1000)

"""y0 = np.ones(1000)
y1 = x
y2 = 2*x*x - 1
y3 = 2*x*(y2)-x
y4 = 2*x*y3 - y2

plt.plot(x, y0, label='$T_0 (x)$')
plt.plot(x, y1, color='r', label='$T_1 (x)$')
plt.plot(x, y2, color='g', label='$T_2 (x)$')
plt.plot(x, y3, color='me', label='$T_3 (x)$')
plt.plot(x, y4, color='c', label='$T_4 (x)$')"""

y0 = np.ones(1000)
y1 = x
y2 = (1/(2))*((2*1 + 1)*x*y1 - 1*y0)
y3 = (1/(3))*((2*2 + 1)*x*y2 - 2*y1)
y4 = (1/(4))*((2*3 + 1)*x*y3 - 3*y2)

plt.plot(x, y0, label='$P_0 (x)$')
plt.plot(x, y1, color='r', label='$P_1 (x)$')
plt.plot(x, y2, color='g', label='$P_2 (x)$')
plt.plot(x, y3, color='me', label='$P_3 (x)$')
plt.plot(x, y4, color='c', label='$P_4 (x)$')

plt.grid(which='both', alpha=.5, linestyle='--')

plt.legend(loc=4)

plt.show()
