import Code.common.fwhm_computing as fwhm
import numpy as np
import matplotlib.pyplot as plt

# Chebyshev
fwhm_x, fwhm_y, fwhm_z = fwhm.fwhm_poly_interpolation(99, 117, 95, (2, 2, 2), 4, 1, '/media/strangesyd/Seagate/Stage')

slice_x_z = fwhm_x[49, :, :]

hf = plt.figure(figsize=(10, 8), dpi=80)
ha = hf.add_subplot(111, projection='3d')

x = range(slice_x_z.shape[1])
y = range(slice_x_z.shape[0])

X, Y = np.meshgrid(x, y)
im = ha.plot_surface(X, Y, slice_x_z, cmap='gnuplot')

plt.xlabel("$x$ [voxels]")
plt.ylabel("$y$ [voxels]")
ha.set_zlabel("FWHM [mm]")

plt.show()
