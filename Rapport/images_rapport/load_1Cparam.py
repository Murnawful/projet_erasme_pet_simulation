import numpy as np
from matplotlib import pyplot as plt


at = "TOF"
ty = "real"
frame = "_frame"
atlas = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_param1C_atlas_max.npy")

m = 10**(-4)

plt.rcParams["figure.figsize"] = (10, 20)
plt.rcParams.update({'font.size': 16})

plt.imshow(atlas[48, :, :, 1])
plt.xlabel("x [voxels]")
plt.ylabel("y [voxels]")

cbar = plt.colorbar(label='$k_2$ [s$^{-1}$]')

plt.show()
