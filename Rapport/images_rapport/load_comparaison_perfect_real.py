import numpy as np
from matplotlib import pyplot as plt
import common.visu as v
import SimpleITK as sitk

at = "TOF"
ty = "real"
frame = "_frame"

tac_real = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_TAC_1C_real" + frame + ".npy")
tac_noiseless = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_TAC_1C_noiseless" + frame + ".npy")

t_ = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_TAC_1C_time.npy")
coor = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_TAC_1C_coor.npy")
ind = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_prob_atlas_ind_max.npy")
atlas = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_param1C_atlas_max.npy")

np.random.seed(75746)
num = np.random.randint(0, tac_real.shape[0])

fig, ax = plt.subplots(1, 2)

temp_real = np.zeros(ind.shape)
temp_perfect = np.zeros(ind.shape)

# z, y, x = coor[0][num], coor[1][num], coor[2][num]
z, y, x = 48, 56, 55
for i in range(len(coor[0][:])):
    temp_real[coor[0][i], coor[1][i], coor[2][i]] = tac_real[i, 900]
    temp_perfect[coor[0][i], coor[1][i], coor[2][i]] = tac_noiseless[i, 900]

for i in range(len(coor[0])):
    if coor[0][i] == z and coor[1][i] == y and coor[2][i] == x:
        num = i

plt.rcParams["figure.figsize"] = (10, 20)
plt.rcParams.update({'font.size': 16})

# temp_real[48, :, :]
im = []
im.append(ax[0].imshow(temp_perfect[48, :, :], vmin=0, vmax=200, cmap='gray'))
im.append(ax[1].imshow(temp_real[48, :, :], vmin=0, vmax=800, cmap='gray'))
i = 0
for a in ax.flat:
    a.set(xlabel='$x$ [voxels]', ylabel='$y$ [voxels]')
    fig.colorbar(im[i], ax=a, label='SUV', shrink=.6)
    i += 1


plt.show()
