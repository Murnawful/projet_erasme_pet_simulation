import numpy as np
from matplotlib import pyplot as plt


at = "TOF"
ty = "real"
frame = "_frame"
ind = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_prob_atlas_ind_max.npy")

m = 5

print(ind.shape)

plt.rcParams["figure.figsize"] = (10, 20)
plt.rcParams.update({'font.size': 16})

plt.imshow(ind[48, :, :], cmap=plt.cm.get_cmap('gnuplot', m))
plt.xlabel("x [voxels]")
plt.ylabel("y [voxels]")

cbar = plt.colorbar(ticks=[0.4, 1.2, 2, 2.8, 3.6])
cbar.set_ticklabels(['Empty', 'White Matter', 'Gray Matter', 'CSF', 'Vessels'])

plt.show()
