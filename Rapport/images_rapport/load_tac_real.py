import numpy as np
from matplotlib import pyplot as plt
import common.visu as v
import SimpleITK as sitk

at = "TOF"
ty = "real"
frame = "_frame"

tac_real = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_TAC_1C_real" + frame + ".npy")

t_ = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_TAC_1C_time.npy")

# 7854613
np.random.seed(986662)
num = np.random.randint(0, tac_real.shape[0])


plt.rcParams["figure.figsize"] = (50, 50)
plt.rcParams.update({'font.size': 20})

plt.plot(t_, tac_real[num])
plt.grid()
plt.xlabel("Time [s]")
plt.ylabel("SUV")

plt.show()
