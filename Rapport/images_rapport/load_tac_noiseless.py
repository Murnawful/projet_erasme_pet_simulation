import numpy as np
from matplotlib import pyplot as plt
import common.visu as v
import SimpleITK as sitk

at = "TOF"
ty = "real"
frame = "_frame"

tac_noiseless = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_TAC_1C_noiseless" + frame + ".npy")

t_ = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_TAC_1C_time.npy")
coor = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_TAC_1C_coor.npy")

# 7854613
np.random.seed(986662)
num = np.random.randint(0, tac_noiseless.shape[0])

print((coor[0][num], coor[1][num], coor[2][num]))

plt.rcParams["figure.figsize"] = (50, 50)
plt.rcParams.update({'font.size': 20})

plt.plot(t_, tac_noiseless[num])
plt.grid()
plt.xlabel("Time [s]")
plt.ylabel("SUV")

plt.show()
