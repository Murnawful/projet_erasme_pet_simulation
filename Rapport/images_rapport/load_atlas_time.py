import numpy as np
from matplotlib import pyplot as plt
import common.visu as v
import SimpleITK as sitk


at = "TOF"
ty = "real"
frame = "_frame"

tac_real = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_TAC_1C_real" + frame + ".npy")
tac_noiseless = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_TAC_1C_noiseless" + frame + ".npy")

coor = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_TAC_1C_coor.npy")
ind = np.load("/media/strangesyd/Seagate/Stage/Atlas/" + at + "/" + at + "_prob_atlas_ind_max.npy")

np.random.seed(75746)
num = np.random.randint(0, tac_real.shape[0])

temp_real = np.zeros(ind.shape)

fig, ax = plt.subplots(2, 2)

for i in range(len(coor[0][:])):
    temp_real[coor[0][i], coor[1][i], coor[2][i]] = tac_real[i, 20]

m = 1600

plt.rcParams["figure.figsize"] = (50, 50)
plt.rcParams.update({'font.size': 16})

slice = 48

ax[0, 0].imshow(temp_real[slice, :, :], vmin=0, vmax=m, cmap='gray')

for i in range(len(coor[0][:])):
    temp_real[coor[0][i], coor[1][i], coor[2][i]] = tac_real[i, 100]

ax[0, 1].imshow(temp_real[slice, :, :], vmin=0, vmax=m, cmap='gray')

for i in range(len(coor[0][:])):
    temp_real[coor[0][i], coor[1][i], coor[2][i]] = tac_real[i, 500]

ax[1, 0].imshow(temp_real[slice, :, :], vmin=0, vmax=m, cmap='gray')

for i in range(len(coor[0][:])):
    temp_real[coor[0][i], coor[1][i], coor[2][i]] = tac_real[i, 900]

im = ax[1, 1].imshow(temp_real[slice, :, :], vmin=0, vmax=m, cmap='gray')

for a in ax.flat:
    a.set(xlabel='$x$ [voxels]', ylabel='$y$ [voxels]')

fig.subplots_adjust(right=0.8)
cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
fig.colorbar(im, cax=cbar_ax, label='SUV')

plt.show()
