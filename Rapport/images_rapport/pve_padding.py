import numpy as np
from scipy import signal
import math as m
import matplotlib.pyplot as plt


def kernel3d(radius, fwhm_x, fwhm_y, fwhm_z, spacing):
    """ Fonction retournant un kernel en 3D de différentes FWHM selon les 3 directions de l'espace """

    num = 2 * radius + 1

    std_x = (fwhm_x / (2 * m.sqrt(2 * m.log(2)))) / spacing[0]
    std_y = (fwhm_y / (2 * m.sqrt(2 * m.log(2)))) / spacing[1]
    std_z = (fwhm_z / (2 * m.sqrt(2 * m.log(2)))) / spacing[2]

    kernel_1d_x = signal.gaussian(num, std=std_x)
    kernel_1d_y = signal.gaussian(num, std=std_y)
    kernel_1d_z = signal.gaussian(num, std=std_z)
    kernel_2d = np.outer(kernel_1d_y, kernel_1d_x)
    kernel_3d = np.outer(kernel_2d, kernel_1d_z).reshape(num, num, num)
    kernel_3d = np.expand_dims(kernel_3d, -1)
    return kernel_3d


single = np.zeros((9, 9, 9))
single[4, 4, 4] = 1
nt, nx, ny, nz = 1, 9, 9, 9

spacing = (2, 2, 2)

tseries = {(4, 4, 4): 1}

me = 2

cag_4dp = np.zeros((nx + 2 * me, ny + 2 * me, nz + 2 * me, nt))

mm = 2 * me + 1  # kernel width
for (i, j, k), tdata in tseries.items():
    kernel = kernel3d(me, 6, 6, 6, spacing)
    # convolution of one voxel by kernel is trivial.
    # slice4d_c has shape (mm, mm, mm, nt).
    slice4d_c = kernel * tdata
    if (i, j, k) == (4, 4, 4):
        print(kernel)
    cag_4dp[k:k + mm, j:j + mm, i:i + mm, :] += slice4d_c

cag_4d = cag_4dp[me:-me, me:-me, me:-me, :]

plt.rcParams["figure.figsize"] = (10, 20)
plt.rcParams.update({'font.size': 16})

fig, ax = plt.subplots(1, 5)

im = [ax[0].imshow(cag_4d[2, :, :, 0], vmin=0, vmax=1), ax[1].imshow(cag_4d[3, :, :, 0], vmin=0, vmax=1),
      ax[2].imshow(cag_4d[4, :, :, 0], vmin=0, vmax=1),
      ax[3].imshow(cag_4d[5, :, :, 0], vmin=0, vmax=1), ax[4].imshow(cag_4d[6, :, :, 0], vmin=0, vmax=1)]

gov = im[-1]

for a in ax.flat:
    a.set(xlabel='$x$ [voxels]', ylabel='$y$ [voxels]')

fig.colorbar(im[2], ax=ax, orientation='horizontal', fraction=.1)

plt.subplots_adjust(wspace=.5, bottom=.3)

plt.show()

"""# coor: coordonnées des voxels non-vides
# curves: les courbes auxquelles ajouter du PVE
# ind: tableau des indices de tissus
# me: largeur de padding, amplitude en voxels du kernel
# fwhm: tuple contenant les arrays des FWHM dans chaque direction de l'espace, pour chaque voxel
# spacing: tuple contenant l'écart entre le centre de chaque voxel selon les 3 directions de l'espace

# Récupération des longueur des vecteurs
print("Adding PVE to curves...")
nx, ny, nz, nt, nnz = sh[2], sh[1], sh[0], curves.shape[-1], curves.shape[0]
# Création d'un dictionnaire pour regrouper les courbes et leurs coordonnées dans la grille
tseries = {}

# Remplissage du tableau: en keys les coordonnées de cette courbe dans la grille, en values les données temporelles
# de cette courbe
for i in range(nnz):
    # Parcours de chacune des courbes
    while True:
        # Récupère les coordonnées de la courbe !! la conversion au format (x, y, z) se fait ici !!
        ijk = (coor[2, i], coor[1, i], coor[0, i])
        if ijk not in tseries:
            # Associe une courbe à ses coordonnées
            tseries[ijk] = curves[i, :]
            break

cag_4dp = np.zeros((nx + 2 * me, ny + 2 * me, nz + 2 * me, nt))

mm = 2 * me + 1  # kernel width
for (i, j, k), tdata in tseries.items():
    kernel = kernel3d(me, fwhm[0][k, j, i], fwhm[1][k, j, i], fwhm[2][k, j, i], spacing)
    # convolution of one voxel by kernel is trivial.
    # slice4d_c has shape (mm, mm, mm, nt).
    slice4d_c = kernel * tdata

    cag_4dp[k:k + mm, j:j + mm, i:i + mm, :] += slice4d_c

cag_4d = cag_4dp[me:-me, me:-me, me:-me, :]"""
